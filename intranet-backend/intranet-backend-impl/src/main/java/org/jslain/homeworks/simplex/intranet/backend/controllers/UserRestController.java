package org.jslain.homeworks.simplex.intranet.backend.controllers;

import lombok.extern.slf4j.Slf4j;
import org.jslain.homeworks.simplex.core.entities.UserInfoEntity;
import org.jslain.homeworks.simplex.intranet.backend.config.AuthServerConfig;
import org.jslain.homeworks.simplex.intranet.backend.dto.UserDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.UserRegistrationRequest;
import org.jslain.homeworks.simplex.intranet.backend.repositories.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("user")
@Slf4j
public class UserRestController {

  @Autowired
  private UserInfoRepository userInfoRepository;

  @Autowired
  @Qualifier("userDetailsService")
  private UserDetailsManager userDetailsManager;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @GetMapping
  @Secured("ROLE_USER")
  public UserDto getCurrentUser(Authentication authentication) throws IllegalStateException{
    UserInfoEntity userInfoEntity = userInfoRepository.findById(authentication.getName())
    .orElseThrow(() -> new IllegalStateException(String.format("No user_info found for user '%s'", authentication.getName())));

    return toDto(userInfoEntity);
  }

  @GetMapping("/all")
  @Secured("ROLE_ADMIN")
  public List<UserDto> getAllUsers() throws IllegalStateException{
    return userInfoRepository.findAll()
      .stream()
      .map(this::toDto)
      .collect(Collectors.toList());
  }

  @PostMapping
  @Secured("ROLE_ADMIN")
  public void createNewUser(@RequestBody UserRegistrationRequest userRegistrationRequest){
    UserDetails userDetails = new User(
      userRegistrationRequest.getUsername(),
      passwordEncoder.encode(userRegistrationRequest.getPassword()),
      AuthServerConfig.EMPLOYEE_DEFAULT_AUTHORITIES);

    userDetailsManager.createUser(userDetails);

    UserInfoEntity userInfo = toEntity(
      userRegistrationRequest.getUsername(),
      userRegistrationRequest.getUserInfo());

    userInfoRepository.save(userInfo);
  }

  private UserDto toDto(UserInfoEntity userInfoEntity){
    List<String> authorities = userDetailsManager.loadUserByUsername(userInfoEntity.getUsername())
      .getAuthorities().stream()
      .map(GrantedAuthority::getAuthority)
      .collect(Collectors.toList());

    return UserDto.builder()
      .username(userInfoEntity.getUsername())
      .firstName(userInfoEntity.getFirstName())
      .lastName(userInfoEntity.getLastName())
      .address(userInfoEntity.getAddress())
      .phoneNumber(userInfoEntity.getPhoneNumber())
      .authorities(authorities)
      .build();
  }

  private UserInfoEntity toEntity(String username, UserDto userDto) {
    return UserInfoEntity.builder()
      .username(username)
      .firstName(userDto.getFirstName())
      .lastName(userDto.getLastName())
      .address(userDto.getAddress())
      .phoneNumber(userDto.getPhoneNumber())
      .build();
  }
}
