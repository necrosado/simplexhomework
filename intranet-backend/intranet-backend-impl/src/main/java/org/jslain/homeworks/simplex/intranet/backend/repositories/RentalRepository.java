package org.jslain.homeworks.simplex.intranet.backend.repositories;

import org.jslain.homeworks.simplex.core.entities.Rental;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RentalRepository extends JpaRepository<Rental, Long> {
}
