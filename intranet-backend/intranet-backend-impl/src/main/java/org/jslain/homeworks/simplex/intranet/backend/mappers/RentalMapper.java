package org.jslain.homeworks.simplex.intranet.backend.mappers;

import org.jslain.homeworks.simplex.core.entities.Rental;
import org.jslain.homeworks.simplex.core.entities.RentalItem;
import org.jslain.homeworks.simplex.core.entities.RentalStatus;
import org.jslain.homeworks.simplex.core.entities.ToolInstance;
import org.jslain.homeworks.simplex.intranet.backend.dto.CustomerDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.RentalDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolInstanceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

@Component
public class RentalMapper {

  public RentalDto fromRental(Rental rental) {
    RentalDto rentalDto = RentalDto.builder()
      .id(rental.getIdRental())
      .customer(CustomerDto.builder()
        .address(rental.getCustomer().getAddress())
        .firstName(rental.getCustomer().getFirstName())
        .lastName(rental.getCustomer().getLastName())
        .phoneNumber(rental.getCustomer().getPhoneNumber())
        .build())
      .startDate(rental.getRentalItems().stream()
        .map(RentalItem::getStartDate)
        .filter(Objects::nonNull)
        .map(ZonedDateTime::toLocalDate)
        .min(Comparator.comparing(LocalDate::toEpochDay)).orElse(null))
      .endDate(rental.getRentalItems().stream()
        .map(RentalItem::getEndDate)
        .filter(Objects::nonNull)
        .map(ZonedDateTime::toLocalDate)
        .max(Comparator.comparing(LocalDate::toEpochDay)).orElse(null))
      .toolInstances(
        rental.getRentalItems().stream()
          .map(ri -> {return ToolInstanceDto.builder()
            .distinctiveName(ri.getToolInstance().getDistinctiveName())
            .unavailableSince(Optional.ofNullable(ri.getStartDate()))
            .build();})
          .collect(Collectors.toList())
      )
      .build();

    return rentalDto;
  }
}
