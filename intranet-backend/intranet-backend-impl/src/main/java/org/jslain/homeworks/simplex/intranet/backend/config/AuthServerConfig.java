package org.jslain.homeworks.simplex.intranet.backend.config;

import lombok.Builder;
import org.jslain.homeworks.simplex.core.entities.Tool;
import org.jslain.homeworks.simplex.core.entities.UserInfoEntity;
import org.jslain.homeworks.simplex.core.utils.SearchUtils;
import org.jslain.homeworks.simplex.intranet.backend.repositories.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.JdbcUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

@Configuration
@EnableGlobalMethodSecurity(
  securedEnabled = true,
  jsr250Enabled = true,
  prePostEnabled = true)
@EnableWebSecurity
@EntityScan(basePackageClasses = {Tool.class})
public class AuthServerConfig extends WebSecurityConfigurerAdapter {

  public static final String ROLE_EMPLOYEE = "ROLE_USER";
  public static final String ROLE_ADMIN = "ROLE_ADMIN";

  public static final List<GrantedAuthority> EMPLOYEE_DEFAULT_AUTHORITIES = Arrays.asList(
    new SimpleGrantedAuthority(ROLE_EMPLOYEE)
  );

  @Autowired
  DataSource ds;

  @Autowired
  private UserInfoRepository userInfoRepository;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .logout().permitAll()
      .and().formLogin().successHandler(new MySavedRequestAwareAuthenticationSuccessHandler())
      .failureHandler(new SimpleUrlAuthenticationFailureHandler())
      .and().exceptionHandling().authenticationEntryPoint(new Http403ForbiddenEntryPoint())
      .and()
      .authorizeRequests()
      .antMatchers("/", "/actuator/**", "/h2-console/*").permitAll()
      .antMatchers("/login").permitAll()
      .and().exceptionHandling().authenticationEntryPoint(new Http403ForbiddenEntryPoint());

    http.csrf().disable();

    http.headers().frameOptions().sameOrigin();
  }

  @Autowired
  public void configureGlobalInitial(
    AuthenticationManagerBuilder authenticationMgr,
    ApplicationContext springContext,
    @Qualifier("initUserDetailServicenConfigurer")
      Consumer<JdbcUserDetailsManagerConfigurer> initUserDetailServicenConfigurer) throws Exception {
    JdbcUserDetailsManagerConfigurer jdbcConfigurer = initUserDetailService(authenticationMgr, springContext);

    initUserDetailServicenConfigurer.accept(jdbcConfigurer);
  }

  @Bean("initUserDetailServicenConfigurer")
  @Profile("!initial")
  public Consumer<JdbcUserDetailsManagerConfigurer> initUserDetailServicenConfigurerNoop(){
    return (u) -> {};
  }
  @Bean("initUserDetailServicenConfigurer")
  @Profile("initial")
  public Consumer<JdbcUserDetailsManagerConfigurer> initUserDetailServicenConfigurer(){
    return (jdbcConfigurer) -> {
      this.initDatabaseFirstTimeOnly(jdbcConfigurer);
    };
  }

  private JdbcUserDetailsManagerConfigurer initUserDetailService(
    AuthenticationManagerBuilder authenticationMgr,
    ApplicationContext springContext)
  throws Exception{
    JdbcUserDetailsManagerConfigurer jdbcConfigurer = authenticationMgr
      .jdbcAuthentication()
      .dataSource(ds);

    authenticationMgr.userDetailsService(jdbcConfigurer.getUserDetailsService());

    ConfigurableListableBeanFactory beanFactory = ((ConfigurableApplicationContext) springContext).getBeanFactory();
    beanFactory.registerSingleton("userDetailsService", jdbcConfigurer.getUserDetailsService());

    return jdbcConfigurer;
  }

  private void initDatabaseFirstTimeOnly(JdbcUserDetailsManagerConfigurer jdbcConfigurer) {

    try {
      ds.getConnection().createStatement()
        .execute("DROP TABLE USERS;");
    }catch (SQLException e){}

    try {
      ds.getConnection().createStatement()
        .execute("DROP TABLE AUTHORITIES;");
    }catch (SQLException e){}


      jdbcConfigurer
        .withDefaultSchema()
        .withUser("employee")
        .password(encoder().encode("employee"))
        .authorities(ROLE_EMPLOYEE)
        .and()
        .withUser("admin")
        .password(encoder().encode("admin"))
        .authorities(ROLE_EMPLOYEE, ROLE_ADMIN);

      userInfoRepository.save(UserInfoEntity.builder()
        .username("employee")
        .firstName("EmployeeFirstName")
        .lastName("EmployeeLastName")
        .address("Employee Address")
        .phoneNumber("418-555-1212")
        .build());

      userInfoRepository.save(UserInfoEntity.builder()
        .username("admin")
        .firstName("Admin-FirstName")
        .lastName("Admin-LastName")
        .address("Admin Address")
        .phoneNumber("418-555-2323")
        .build());
  }

  @Bean()
  public PasswordEncoder  encoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public SearchUtils searchUtils() {
    return new SearchUtils();
  }
}
