package org.jslain.homeworks.simplex.intranet.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = {IntranetBackendApplication.class})
public class IntranetBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntranetBackendApplication.class, args);
	}

}
