package org.jslain.homeworks.simplex.intranet.backend;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Order(0)
@Slf4j
public class IntranetBackendApplicationListener implements ApplicationListener<ApplicationReadyEvent> {

  @Override
  public void onApplicationEvent(ApplicationReadyEvent event) {
    try {
      for(String line : IOUtils.readLines(IntranetBackendApplicationListener.class
        .getResourceAsStream("/application-started.txt"), "UTF-8")) {
        log.info(line);
      }
    }catch (IOException e) {
      log.error(e.getMessage());
    }
  }

}
