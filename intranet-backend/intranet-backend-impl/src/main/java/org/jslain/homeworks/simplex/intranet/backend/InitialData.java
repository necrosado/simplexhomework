package org.jslain.homeworks.simplex.intranet.backend;

import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.crypto.tls.TlsFatalAlert;
import org.jslain.homeworks.simplex.core.entities.Image;
import org.jslain.homeworks.simplex.core.entities.Tool;
import org.jslain.homeworks.simplex.intranet.backend.controllers.CustomersRestController;
import org.jslain.homeworks.simplex.intranet.backend.dto.CustomerDto;
import org.jslain.homeworks.simplex.intranet.backend.repositories.CustomersRepository;
import org.jslain.homeworks.simplex.intranet.backend.repositories.ImageRepository;
import org.jslain.homeworks.simplex.intranet.backend.repositories.ToolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.Locale;

@Component
@Profile("initial")
@Slf4j
public class InitialData {

  @Autowired
  private CustomersRestController customersRestController;

  @Autowired
  private ImageRepository imageRepository;

  @Autowired
  private ToolRepository toolRepository;

  @EventListener
  public void onApplicationEvent(ContextRefreshedEvent contextStartedEvent) throws IOException{
    Faker faker = new Faker(new Locale("ca"));
    Faker fakerFr = new Faker(new Locale("fr"));


    for(int i=0 ; i<100 ; ++i) {
      CustomerDto customerDto = new CustomerDto();
      customerDto.setFirstName(fakerFr.name().firstName());
      customerDto.setLastName(fakerFr.name().lastName());
      customerDto.setPhoneNumber(faker.phoneNumber().cellPhone());
      customerDto.setAddress(faker.address().fullAddress());

      log.info("Creating customer: {}", customerDto);

      customersRestController.save(customerDto);
    }

    saveImage("ECHELLE DE 30 PIEDS", "echelle.png");
  }

  private void saveImage(String toolName, String resourceName) throws IOException {
    Tool tool = this.toolRepository.findByName(toolName).get(0);
    this.toolRepository.findAll();
    Image image = new Image();
    image.setDisplayOrder(1);
    image.setFilename(resourceName);
    image.setTool(tool);
    image.setData(IOUtils.toByteArray(InitialData.class.getResourceAsStream("/initialData/" + resourceName)));

    this.imageRepository.save(image);
  }
}
