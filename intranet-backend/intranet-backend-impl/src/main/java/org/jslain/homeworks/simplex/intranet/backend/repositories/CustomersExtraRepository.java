package org.jslain.homeworks.simplex.intranet.backend.repositories;

import io.micrometer.core.instrument.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.jslain.homeworks.simplex.core.entities.Customer;
import org.jslain.homeworks.simplex.core.utils.SearchUtils;
import org.jslain.homeworks.simplex.intranet.backend.dto.FindCustomersRequestParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Component
public class CustomersExtraRepository {
  @Autowired
  private EntityManager entityManager;

  private Session getSession() {
    return entityManager.unwrap(Session.class);
  }
  @Autowired
  private SearchUtils searchUtils;

  public List<Customer> search(FindCustomersRequestParams params) {
    Session s = getSession();
    CriteriaBuilder cb = s.getCriteriaBuilder();
    CriteriaQuery<Customer> query = cb.createQuery(Customer.class);
    Root<Customer> root = query.from(Customer.class);

    query = query.select(root);

    List<Predicate> predicates = new ArrayList<>();

    if(StringUtils.isNotEmpty(params.getFirstName())) {
      predicates.add(
        cb.like(
          root.get("firstNameSearch"),
          String.format("%%%s%%", searchUtils.toSearchString(params.getFirstName()))));
    }

    if(StringUtils.isNotEmpty(params.getLastName())) {
      predicates.add(
        cb.like(
          root.get("lastNameSearch"),
          String.format("%%%s%%", searchUtils.toSearchString(params.getLastName()))));
    }

    if(StringUtils.isNotEmpty(params.getPhoneNumber())) {
      predicates.add(
        cb.like(
          root.get("phoneNumberSearch"),
          String.format("%%%s%%", searchUtils.toSearchString(params.getPhoneNumber()))));
    }

    if(StringUtils.isNotEmpty(params.getAddress())) {
      predicates.add(
        cb.like(
          root.get("addressSearch"),
          String.format("%%%s%%", searchUtils.toSearchString(params.getAddress()))));
    }

    query = query.where(cb.and(predicates.toArray(new Predicate[]{})));

    Query<Customer> criteria = s.createQuery(query);

    return criteria.list();
  }
}
