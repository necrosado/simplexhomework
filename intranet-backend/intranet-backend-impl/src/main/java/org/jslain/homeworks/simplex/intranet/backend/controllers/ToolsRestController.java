package org.jslain.homeworks.simplex.intranet.backend.controllers;

import org.jslain.homeworks.simplex.core.entities.*;
import org.jslain.homeworks.simplex.core.utils.SearchUtils;
import org.jslain.homeworks.simplex.intranet.backend.config.AuthServerConfig;
import org.jslain.homeworks.simplex.intranet.backend.dto.FindToolsRequestParams;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolImageDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolInstanceDto;
import org.jslain.homeworks.simplex.intranet.backend.mappers.ToolMapper;
import org.jslain.homeworks.simplex.intranet.backend.repositories.ImageRepository;
import org.jslain.homeworks.simplex.intranet.backend.repositories.ToolInstanceRepository;
import org.jslain.homeworks.simplex.intranet.backend.repositories.ToolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("tools")
public class ToolsRestController {

  private ToolRepository toolRepository;

  private ToolInstanceRepository toolInstanceRepository;

  private ImageRepository imageRepository;

  private SearchUtils searchUtils;

  private ToolMapper toolMapper;

  @Autowired
  public ToolsRestController(ToolRepository toolRepository,
                             ToolInstanceRepository toolInstanceRepository,
                             ImageRepository imageRepository,
                             SearchUtils searchUtils,
                             ToolMapper toolMapper) {
    this.toolRepository = toolRepository;
    this.toolInstanceRepository = toolInstanceRepository;
    this.imageRepository = imageRepository;
    this.searchUtils = searchUtils;
    this.toolMapper = toolMapper;
  }

  @PostMapping("search")
  @Secured(AuthServerConfig.ROLE_EMPLOYEE)
  public List<ToolDto> findTools(@RequestBody FindToolsRequestParams params) {
    return toolRepository.findByName(searchUtils.toSearchString(params.getToolName()))
      .stream()
      .map(toolMapper::fromTool)
      .collect(Collectors.toList());
  }

  @PostMapping("/")
  public Long post(
    @RequestBody ToolDto toolDto) {
    Tool tool = new Tool();

    tool.setName(toolDto.getToolName());
    tool.setNameSearch(searchUtils.toSearchString(toolDto.getToolName()));
    tool.setDescription(toolDto.getToolDescription());
    tool.setDescriptionSearch(searchUtils.toSearchString(toolDto.getToolDescription()));
    tool.setPrice(toolDto.getPrice());

    toolDto.getImage().ifPresent((toolImageDto) -> {
      Image newImage = Image.builder()
        .tool(tool)
        .data(toolImageDto.getData())
        .filename(toolImageDto.getFilename())
        .displayOrder(toolImageDto.getDisplayOrder())
        .build();

      imageRepository.save(newImage);
    });

    return toolRepository.save(tool).getIdTool();
  }


  @PutMapping("/{id}")
  public Long put(
    @PathVariable("id") Long id,
    @RequestBody ToolDto toolDto) {
    Tool tool = toolRepository.getOne(id);

    tool.setName(toolDto.getToolName());
    tool.setNameSearch(searchUtils.toSearchString(toolDto.getToolName()));
    tool.setDescription(toolDto.getToolDescription());
    tool.setDescriptionSearch(searchUtils.toSearchString(toolDto.getToolDescription()));
    tool.setPrice(toolDto.getPrice());

    if(toolDto.getImage().isPresent()) {
      ToolImageDto toolImageDto = toolDto.getImage().get();
      Image.ImageBuilder imageBuilder;
      if (!tool.getImagesIds().isEmpty()) {
        imageBuilder = tool.getImagesIds().iterator().next().toBuilder();
      } else {
        imageBuilder = Image.builder();
      }

      Image imageToSave = imageBuilder
        .tool(tool)
        .data(toolImageDto.getData())
        .filename(toolImageDto.getFilename())
        .displayOrder(toolImageDto.getDisplayOrder())
        .build();

      imageRepository.save(imageToSave);
    } else {
      if (!tool.getImagesIds().isEmpty()) {
        tool.getImagesIds().stream().forEach(
          img -> imageRepository.delete(img)
        );
      }
    }

    for(ToolInstanceDto toolInstanceDto : toolDto.getToolInstances()) {
      ToolInstance toolInstance;
      if(toolInstanceDto.getToolInstanceId() != null) {
        toolInstance = toolInstanceRepository.getOne(toolInstanceDto.getToolInstanceId());
      } else {
        toolInstance = new ToolInstance();
      }

      toolInstance = toolInstance.toBuilder()
        .distinctiveName(toolInstanceDto.getDistinctiveName())
        .distinctiveNameSearch(this.searchUtils.toSearchString(toolInstanceDto.getDistinctiveName()))
        .tool(tool)
        .build();

      toolInstanceRepository.save(toolInstance);
    }

    return toolRepository.save(tool).getIdTool();
  }

  @GetMapping("/{id}")
  public ToolDto get(
    @PathVariable("id") Long id) {
    Tool t = toolRepository.getOne(id);

    return toolMapper.fromTool(t);
  }
}
