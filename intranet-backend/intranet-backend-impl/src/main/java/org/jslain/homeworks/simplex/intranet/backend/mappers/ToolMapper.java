package org.jslain.homeworks.simplex.intranet.backend.mappers;

import org.jslain.homeworks.simplex.core.entities.*;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolImageDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolInstanceDto;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ToolMapper {
  public ToolDto fromTool(Tool t) {
    return ToolDto.builder()
      .toolId(t.getIdTool())
      .toolName(t.getName())
      .toolDescription(t.getDescription())
      .price(t.getPrice())
      .toolInstances(t.getToolInstances()
        .stream()
        .map((toolInstance) -> ToolInstanceDto.builder()
          .toolInstanceId(toolInstance.getIdToolInstance())
          .distinctiveName(toolInstance.getDistinctiveName())
          .customer(getCurrentOrNextRent(toolInstance)
            .map(RentalItem::getRental)
            .map(Rental::getCustomer)
            .map(customer -> {return String.format("%s %s", customer.getFirstName(), customer.getLastName());}))
          .unavailableSince(getCurrentOrNextRent(toolInstance).map(RentalItem::getStartDate))
          .rentalId(getCurrentOrNextRent(toolInstance)
            .map(RentalItem::getRental)
            .map(Rental::getIdRental))
          .build())
        .collect(Collectors.toList()))
      .image(getToolImage(t))
      .build();
  }

  private Optional<ToolImageDto> getToolImage(Tool tool) {
    Optional<ToolImageDto> toolImage;
    if(tool.getImagesIds().isEmpty()) {
      toolImage = Optional.empty();
    } else {
      Image img = tool.getImagesIds().iterator().next();
      toolImage = Optional.of(
        ToolImageDto.builder()
          .data(img.getData())
          .filename(img.getFilename())
          .idImage(img.getIdImage())
          .toolId(tool.getIdTool())
          .build());
    }

    return toolImage;
  }

  private Optional<RentalItem> getCurrentOrNextRent(ToolInstance toolInstance) {
    return toolInstance
      .getRentalItems()
      .stream()
      .filter((ri) -> {return ri.getStartDate().isAfter(ZonedDateTime.now())
        || ri.getEndDate() == null;})
      .sorted(Comparator.comparing(RentalItem::getStartDate))
      .findFirst();
  }
}
