package org.jslain.homeworks.simplex.intranet.backend.mappers;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import org.jslain.homeworks.simplex.core.entities.RentalStatus;
import org.jslain.homeworks.simplex.core.entities.RentalStatus.Status;
import org.jslain.homeworks.simplex.intranet.backend.dto.RentalStatusDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.RentalStatusType;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Comparator;

@Component
public class RentalStatusMapper {

  private static final BiMap<RentalStatusType, Status> STATUS_MAPPING =
    new ImmutableBiMap.Builder<RentalStatusType, Status>()
    .put(RentalStatusType.NEW, Status.INITIATED)
      .put(RentalStatusType.ACTIVE, Status.CONFIRMED)
      .put(RentalStatusType.TERMINATED, Status.COMPLETED)
      .put(RentalStatusType.CANCELLED, Status.CANCELLED)
    .build();

  public RentalStatusDto fromStatus(RentalStatus rentalStatus) {
    RentalStatusDto rentalStatusDto = new RentalStatusDto();

    rentalStatusDto.setDate(rentalStatus.getDate());
    rentalStatusDto.setStatus(STATUS_MAPPING.inverse().get(rentalStatus.getStatus()));
    rentalStatusDto.setId(rentalStatus.getIdRentalStatus());

    return rentalStatusDto;
  }

  public RentalStatusDto latest(Collection<RentalStatus> rentalStatuses) {
    RentalStatusDto rentalStatutDto = null;

    RentalStatus rs = rentalStatuses.stream().max(Comparator.comparing(RentalStatus::getDate)).orElse(null);

    if(rs != null) {
      rentalStatutDto = fromStatus(rs);
    }

    return rentalStatutDto;
  }
}
