package org.jslain.homeworks.simplex.intranet.backend.controllers;

import org.jslain.homeworks.simplex.core.entities.Customer;
import org.jslain.homeworks.simplex.core.entities.RentalItem;
import org.jslain.homeworks.simplex.core.entities.ToolInstance;
import org.jslain.homeworks.simplex.core.utils.SearchUtils;
import org.jslain.homeworks.simplex.intranet.backend.dto.CustomerDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.FindCustomersRequestParams;
import org.jslain.homeworks.simplex.intranet.backend.dto.RentalSummaryDto;
import org.jslain.homeworks.simplex.intranet.backend.mappers.RentalStatusMapper;
import org.jslain.homeworks.simplex.intranet.backend.repositories.CustomersExtraRepository;
import org.jslain.homeworks.simplex.intranet.backend.repositories.CustomersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("customers")
public class CustomersRestController {

  @Autowired
  private CustomersRepository customersRepository;

  @Autowired
  private CustomersExtraRepository customersExtraRepository;

  @Autowired
  private SearchUtils searchUtils;

  @Autowired
  private RentalStatusMapper rentalStatusMapper;

  @PostMapping("search")
  public List<CustomerDto> search(@RequestBody FindCustomersRequestParams requestParams) {
    return customersExtraRepository.search(requestParams)
      .stream()
      .map(c -> CustomerDto.builder()
        .id(c.getIdCustomer())
        .firstName(c.getFirstName())
        .lastName(c.getLastName())
        .phoneNumber(c.getPhoneNumber())
        .address(c.getAddress())
        .build())
      .collect(Collectors.toList());
  }


  @PostMapping("save")
  public long save(@RequestBody CustomerDto customerDto) {
    Customer.CustomerBuilder customerBuilder;

    if(customerDto.getId() != null) {
      customerBuilder = customersRepository.getOne(customerDto.getId()).toBuilder();
    } else {
      customerBuilder = Customer.builder();
    }

    customerBuilder
      .firstName(customerDto.getFirstName())
      .firstNameSearch(searchUtils.toSearchString(customerDto.getFirstName()))
      .lastName(customerDto.getLastName())
      .lastNameSearch(searchUtils.toSearchString(customerDto.getLastName()))
      .phoneNumber(customerDto.getPhoneNumber())
      .phoneNumberSearch(searchUtils.toSearchString(customerDto.getPhoneNumber()))
      .address(customerDto.getAddress())
      .addressSearch(searchUtils.toSearchString(customerDto.getAddress()));

    return customersRepository.save(customerBuilder.build()).getIdCustomer();
  }

  @GetMapping("{id}/rentals")
  public List<RentalSummaryDto> getCustomerRentals(@PathVariable("id") Long customerId) {
    Customer customer = this.customersRepository.getOne(customerId);
    return customer.getRentals().stream()
      .map(r -> {return RentalSummaryDto
        .builder()
        .id(r.getIdRental())
        .startDate(r.getRentalItems().stream().map(RentalItem::getStartDate).collect(Collectors.minBy(Comparator.naturalOrder())))
        .endDate(r.getRentalItems().stream().map(RentalItem::getEndDate).collect(Collectors.maxBy(Comparator.naturalOrder())))
        .tools(r.getRentalItems().stream()
          .map(RentalItem::getToolInstance)
          .map(ToolInstance::getDistinctiveName)
          .collect(Collectors.joining(", ")))
        .status(rentalStatusMapper.latest(r.getRentalStatuses()).getStatus())
        .build();
      })
      .collect(Collectors.toList());
  }
}
