package org.jslain.homeworks.simplex.intranet.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = {IntranetBackendInitialApplication.class})
public class IntranetBackendInitialApplication {

	public static void main(String[] args) {
    SpringApplication application = new SpringApplication(IntranetBackendInitialApplication.class);

    application.setAdditionalProfiles("initial");

    application.run(args);
	}
}
