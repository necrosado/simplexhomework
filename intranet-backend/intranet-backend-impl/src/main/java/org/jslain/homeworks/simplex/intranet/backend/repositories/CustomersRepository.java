package org.jslain.homeworks.simplex.intranet.backend.repositories;

import org.jslain.homeworks.simplex.core.entities.Customer;
import org.jslain.homeworks.simplex.core.entities.Tool;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface CustomersRepository extends JpaRepository<Customer, Long> {

}
