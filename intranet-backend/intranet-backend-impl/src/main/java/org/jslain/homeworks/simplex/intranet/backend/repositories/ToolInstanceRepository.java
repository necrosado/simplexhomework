package org.jslain.homeworks.simplex.intranet.backend.repositories;

import org.jslain.homeworks.simplex.core.entities.Tool;
import org.jslain.homeworks.simplex.core.entities.ToolInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToolInstanceRepository  extends JpaRepository<ToolInstance, Long> {
}
