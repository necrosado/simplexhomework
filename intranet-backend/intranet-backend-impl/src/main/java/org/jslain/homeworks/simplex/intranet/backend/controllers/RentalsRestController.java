package org.jslain.homeworks.simplex.intranet.backend.controllers;

import org.jslain.homeworks.simplex.core.entities.Customer;
import org.jslain.homeworks.simplex.core.entities.Rental;
import org.jslain.homeworks.simplex.core.entities.RentalItem;
import org.jslain.homeworks.simplex.core.entities.RentalStatus;
import org.jslain.homeworks.simplex.intranet.backend.dto.GetRentalResponseDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.PostRentalRequestDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.RentalDto;
import org.jslain.homeworks.simplex.intranet.backend.exceptions.NotFoundException;
import org.jslain.homeworks.simplex.intranet.backend.mappers.RentalMapper;
import org.jslain.homeworks.simplex.intranet.backend.mappers.RentalStatusMapper;
import org.jslain.homeworks.simplex.intranet.backend.repositories.CustomersExtraRepository;
import org.jslain.homeworks.simplex.intranet.backend.repositories.CustomersRepository;
import org.jslain.homeworks.simplex.intranet.backend.repositories.RentalRepository;
import org.jslain.homeworks.simplex.intranet.backend.repositories.ToolInstanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.Optional;

@RestController("rentals")
@RequestMapping("rentals")
public class RentalsRestController {

  @Autowired
  private RentalRepository rentalRepository;

  @Autowired
  private CustomersRepository customerRepo;

  @Autowired
  private ToolInstanceRepository toolInstanceRepository;

  @Autowired
  private RentalMapper rentalMapper;

  @Autowired
  private RentalStatusMapper rentalStatusMapper;

  @GetMapping("/{id}")
  public GetRentalResponseDto get(@PathVariable("id") Long id) {
    Rental rental = rentalRepository.findById(id).orElseThrow(NotFoundException::new);

    return new GetRentalResponseDto(
      rentalMapper.fromRental(rental),
      rentalStatusMapper.latest(rental.getRentalStatuses()));
  }

  @PostMapping("/{id}/cancel")
  public void cancelRental(@PathVariable("id") Long id) {
    Rental rental = rentalRepository.findById(id).orElseThrow(NotFoundException::new);

    setStatus(rental, RentalStatus.Status.CANCELLED);
    rental.getRentalItems().forEach(ri -> {
      ri.setEndDate(ZonedDateTime.now());
    });

    this.rentalRepository.save(rental);
  }

  @PostMapping("/{id}/terminate")
  public void terminateRental(@PathVariable("id") Long id) {
    Rental rental = rentalRepository.findById(id).orElseThrow(NotFoundException::new);

    setStatus(rental, RentalStatus.Status.COMPLETED);
    rental.getRentalItems().forEach(ri -> {
      ri.setEndDate(ZonedDateTime.now());
    });

    this.rentalRepository.save(rental);
  }

  @PostMapping()
  public Long post(@RequestBody PostRentalRequestDto postRentalRequestDto) {
    Rental rental = new Rental();
    rental.setCustomer(customerRepo.getOne(postRentalRequestDto.getCustomerId()));

    for(Long toollInstanceId : postRentalRequestDto.getToolInstancesIds()) {
      RentalItem rentalItem = new RentalItem();
      rentalItem.setRental(rental);
      rental.getRentalItems().add(rentalItem);

      rentalItem.setStartDate(ZonedDateTime.now());
      rentalItem.setToolInstance(toolInstanceRepository.getOne(toollInstanceId));
    }

    setStatus(rental, RentalStatus.Status.CONFIRMED);

    rental = rentalRepository.save(rental);

    return rental.getIdRental();
  }

  private void setStatus(Rental rental, RentalStatus.Status status) {
    RentalStatus rentalStatus = new RentalStatus();
    rentalStatus.setRental(rental);
    rental.getRentalStatuses().add(rentalStatus);
    rentalStatus.setDate(ZonedDateTime.now());
    rentalStatus.setStatus(status);
  }
}
