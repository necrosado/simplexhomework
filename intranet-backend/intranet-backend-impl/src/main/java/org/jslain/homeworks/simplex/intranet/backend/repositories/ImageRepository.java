package org.jslain.homeworks.simplex.intranet.backend.repositories;

import org.jslain.homeworks.simplex.core.entities.Image;
import org.jslain.homeworks.simplex.core.entities.Tool;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRepository  extends JpaRepository<Image, Long> {
  public List<Image> findByTool(Tool tool);
}
