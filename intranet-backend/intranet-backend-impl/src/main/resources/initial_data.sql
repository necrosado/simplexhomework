/* Tools */
INSERT INTO tool (id_tool, name, name_search, description, description_search, price)
VALUES
(next value for hibernate_sequence, 'Échelle de 30 pieds', 'ECHELLE DE 30 PIEDS', 'Une échelle', 'UNE ECHELLE', 20.00);

/* Tool instances */
INSERT INTO tool_instance (id_tool_instance, distinctive_name, distinctive_name_search, id_tool)
VALUES
(next value for hibernate_sequence, 'Échelle 01', 'ECHELLE 01', (SELECT id_tool FROM tool WHERE name = 'Échelle de 30 pieds')),
(next value for hibernate_sequence, 'Échelle 02', 'ECHELLE 02', (SELECT id_tool FROM tool WHERE name = 'Échelle de 30 pieds'));





















