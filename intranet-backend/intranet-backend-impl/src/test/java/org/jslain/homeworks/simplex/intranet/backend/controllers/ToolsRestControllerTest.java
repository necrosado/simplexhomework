package org.jslain.homeworks.simplex.intranet.backend.controllers;


import org.assertj.core.api.Assert;
import org.assertj.core.api.Assertions;
import org.jslain.homeworks.simplex.core.entities.Image;
import org.jslain.homeworks.simplex.core.entities.Tool;
import org.jslain.homeworks.simplex.core.utils.SearchUtils;
import org.jslain.homeworks.simplex.intranet.backend.dto.FindToolsRequestParams;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolImageDto;
import org.jslain.homeworks.simplex.intranet.backend.mappers.ToolMapper;
import org.jslain.homeworks.simplex.intranet.backend.repositories.ImageRepository;
import org.jslain.homeworks.simplex.intranet.backend.repositories.ToolInstanceRepository;
import org.jslain.homeworks.simplex.intranet.backend.repositories.ToolRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.willAnswer;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.*;

public class ToolsRestControllerTest {

  private static final String FIND_TOOL_NAME = "find_tool param tool_name";
  private static final String TOOL_NAME_CONVERTED_TO_SEARCH_STRING = "tool_name converted_to search_string";

  private static final Long DB_TOOL_ID = 123L;
  private static final String DB_TOOL_NAME = "db tool_name";
  private static final String DB_TOOL_NAME_SEARCH = "db tool_name_search";
  private static final String DB_TOOL_DESCRIPTION = "db tool_description";
  private static final String DB_TOOL_DESCRIPTION_SEARCH = "db tool_description_search";
  private static final BigDecimal DB_TOOL_PRICE = new BigDecimal("10");

  private static final String PARAM_TOOL_NAME = "param tool_name";
  private static final String PARAM_TOOL_NAME_CONVERTED_TO_SEARCH_STRING = "param tool_name converted_to search_string";
  private static final String PARAM_TOOL_DESCRIPTION = "param tool_description";
  private static final String PARAM_TOOL_DESCRIPTION_CONVERTED_TO_SEARCH_STRING = "param tool_description converted_to search_string";

  private FindToolsRequestParams findToolsRequestParams;

  private ToolRepository mockToolRepository = mock(ToolRepository.class);

  private ToolInstanceRepository mockToolInstanceRepository = mock(ToolInstanceRepository.class);

  private ImageRepository mockImageRepository = mock(ImageRepository.class);

  private SearchUtils mockSearchUtils = mock(SearchUtils.class);

  private ToolMapper mockToolMapper = mock(ToolMapper.class);

  private ToolDto toSave;

  ToolsRestController underTest;

  @Before
  public void setup() {
    underTest = new ToolsRestController(
      mockToolRepository,
      mockToolInstanceRepository,
      mockImageRepository,
      mockSearchUtils,
      mockToolMapper
    );

    willReturn(TOOL_NAME_CONVERTED_TO_SEARCH_STRING)
      .given(mockSearchUtils)
      .toSearchString(FIND_TOOL_NAME);
    willReturn(PARAM_TOOL_NAME_CONVERTED_TO_SEARCH_STRING)
      .given(mockSearchUtils)
      .toSearchString(PARAM_TOOL_NAME);
    willReturn(PARAM_TOOL_DESCRIPTION_CONVERTED_TO_SEARCH_STRING)
      .given(mockSearchUtils)
      .toSearchString(PARAM_TOOL_DESCRIPTION);

    this.findToolsRequestParams = FindToolsRequestParams.builder()
      .toolName(FIND_TOOL_NAME)
      .build();

    willAnswer((m) -> m.getArgument(0)).given(mockToolRepository).save(any());

    setupToolToSave();
  }

  private void setupToolToSave() {
    toSave = ToolDto.builder()
      .toolName(PARAM_TOOL_NAME)
      .toolDescription(PARAM_TOOL_DESCRIPTION)
      .image(Optional.empty())
      .toolInstances(new ArrayList<>())
      .build();
  }

  @Test
  public void whenFindTools_givenToolName_thenToolSearchedUsingConvertedName() {
    underTest.findTools(this.findToolsRequestParams);
    verify(mockToolRepository).findByName(TOOL_NAME_CONVERTED_TO_SEARCH_STRING);
  }

  @Test
  public void whenFindTools_givenToolFound_thenMappedToolIsReturned() {
    Tool fromDB = new Tool();
    ToolDto mapped = new ToolDto();
    willReturn(asList(fromDB)).given(mockToolRepository).findByName(anyString());
    willReturn(mapped).given(mockToolMapper).fromTool(fromDB);

    List<ToolDto> result = underTest.findTools(this.findToolsRequestParams);

    assertThat(result).containsExactly(mapped);
  }

  @Test
  public void whenPost_givenToolDtoWithoutImage_thenOnlyToolIsSaved() {
    ToolDto toolDto = new ToolDto();

    underTest.post(toolDto);

    verify(mockToolRepository).save(any(Tool.class));
    verify(mockImageRepository, never()).save(any(Image.class));
  }

  @Test
  public void whenPost_givenToolDtoWithImage_thenToolAndImageIsSaved() {
    ToolDto toolDto = new ToolDto();
    toolDto.setImage(Optional.ofNullable(new ToolImageDto()));

    underTest.post(toolDto);

    verify(mockToolRepository).save(any(Tool.class));
    verify(mockImageRepository).save(any(Image.class));
  }

  @Test
  public void whenPut_givenToolDtoWithoutImage_thenToolIsUpdated() {
    Tool toolToUpdate = Tool.builder()
      .idTool(DB_TOOL_ID)
      .name(DB_TOOL_NAME)
      .nameSearch(DB_TOOL_NAME_SEARCH)
      .imagesIds(new ArrayList<>())
      .build();

    willReturn(toolToUpdate).given(mockToolRepository).getOne(DB_TOOL_ID);
    ArgumentCaptor<Tool> savedToolCaptor = ArgumentCaptor.forClass(Tool.class);
    willReturn(toolToUpdate).given(mockToolRepository).save(savedToolCaptor.capture());

    underTest.put(DB_TOOL_ID, toSave);

    Tool updatedTool = savedToolCaptor.getValue();
    assertThat(updatedTool.getName()).isEqualTo(PARAM_TOOL_NAME);
    assertThat(updatedTool.getNameSearch()).isEqualTo(PARAM_TOOL_NAME_CONVERTED_TO_SEARCH_STRING);
    assertThat(updatedTool.getDescription()).isEqualTo(PARAM_TOOL_DESCRIPTION);
    assertThat(updatedTool.getDescriptionSearch()).isEqualTo(PARAM_TOOL_DESCRIPTION_CONVERTED_TO_SEARCH_STRING);
  }
}
