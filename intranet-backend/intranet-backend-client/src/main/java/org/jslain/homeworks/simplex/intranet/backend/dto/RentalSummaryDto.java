package org.jslain.homeworks.simplex.intranet.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;
import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class RentalSummaryDto {
    private Long id;
    private Optional<ZonedDateTime> startDate;
    private Optional<ZonedDateTime> endDate;
    private String tools;
    private RentalStatusType status;
}
