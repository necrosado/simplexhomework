package org.jslain.homeworks.simplex.intranet.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FindToolsRequestParams {

    private String toolName;
    private boolean availableOnly;
}
