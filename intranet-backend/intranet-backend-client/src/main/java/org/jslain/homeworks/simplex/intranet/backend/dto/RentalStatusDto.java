package org.jslain.homeworks.simplex.intranet.backend.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
public class RentalStatusDto {
    private Long id;
    private RentalStatusType status;
    private ZonedDateTime date;

    public RentalStatusDto(RentalStatusType status) {
        this.status = status;
    }
}
