package org.jslain.homeworks.simplex.intranet.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ToolInstanceDto {
    private Long toolInstanceId;
    private String distinctiveName;
    private Optional<ZonedDateTime> unavailableSince = Optional.empty();
    private Optional<String> customer = Optional.empty();
    private Optional<Long> rentalId = Optional.empty();
}
