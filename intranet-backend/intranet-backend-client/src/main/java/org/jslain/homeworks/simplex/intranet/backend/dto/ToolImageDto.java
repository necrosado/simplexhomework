package org.jslain.homeworks.simplex.intranet.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ToolImageDto {

    private Long toolId;
    private Long idImage;
    private String filename;
    private Integer displayOrder;
    private byte[] data;
}
