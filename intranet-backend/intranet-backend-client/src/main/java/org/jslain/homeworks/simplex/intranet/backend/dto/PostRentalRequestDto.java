package org.jslain.homeworks.simplex.intranet.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class PostRentalRequestDto {

    private Long customerId;
    private LocalDate startDate;
    private List<Long> toolInstancesIds = new ArrayList<>();

}
