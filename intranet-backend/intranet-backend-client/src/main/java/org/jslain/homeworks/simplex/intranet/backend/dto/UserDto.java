package org.jslain.homeworks.simplex.intranet.backend.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
  private String username;
  private String firstName;
  private String lastName;
  private String phoneNumber;
  private String address;

  @Singular
  private List<String> authorities = new ArrayList<>();
}
