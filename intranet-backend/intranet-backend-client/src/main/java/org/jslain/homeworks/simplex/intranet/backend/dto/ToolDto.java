package org.jslain.homeworks.simplex.intranet.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ToolDto {

    private Long toolId;

    private String toolName;

    private String toolDescription;

    private BigDecimal price;

    private List<ToolInstanceDto> toolInstances = new ArrayList<>();

    private Optional<ToolImageDto> image = Optional.empty();
}
