package org.jslain.homeworks.simplex.intranet.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RentalDto {
    private Long id;
    private LocalDate startDate;
    private LocalDate endDate;

    private CustomerDto customer;

    private List<ToolInstanceDto> toolInstances = new ArrayList<>();
}
