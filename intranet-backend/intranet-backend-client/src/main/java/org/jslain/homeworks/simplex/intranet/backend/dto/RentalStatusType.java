package org.jslain.homeworks.simplex.intranet.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RentalStatusType {
    NEW ("new", "New"),
    ACTIVE("active", "Active"),
    TERMINATED("terminated","Terminated"),
    CANCELLED("cancelled", "Cancelled");

    private final String code;
    private final String label;

    public static RentalStatusType fromCode(String code) {
        RentalStatusType found = null;

        for(RentalStatusType status : RentalStatusType.values()){
            if(code.equals(status.code)) {
                found = status;
                break;
            }
        }

        return found;
    }
}
