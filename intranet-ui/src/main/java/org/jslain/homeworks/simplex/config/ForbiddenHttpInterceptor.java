package org.jslain.homeworks.simplex.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import reactor.core.publisher.Mono;

import java.util.Collection;

@Component
public class ForbiddenHttpInterceptor implements ExchangeFilterFunction {

    private Collection<SessionExpiredAware> sessionExpiredAwares;

    @Autowired
    @Lazy
    public void setSessionExpiredAwares(Collection<SessionExpiredAware> sessionExpiredAwares) {
        this.sessionExpiredAwares = sessionExpiredAwares;
    }

    @Override
    public Mono<ClientResponse> filter(ClientRequest clientRequest, ExchangeFunction next) {
        return next.exchange(clientRequest)
                .doOnError(HttpClientErrorException.class, (th) -> {
                    if(th.getStatusCode() == HttpStatus.FORBIDDEN) {
                        sessionExpiredAwares.stream().forEach(sea -> sea.onSessionExpired());
                    }
                })
                .doOnError(HttpServerErrorException.class, th -> {
                    System.out.println(th);
                });
    }
}
