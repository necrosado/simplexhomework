package org.jslain.homeworks.simplex.tabs.tools;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolImageDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CurrentTool {

    private ToolDto tool = new ToolDto();

    public boolean isNew() {
        return tool.getToolId() == null;
    }
}
