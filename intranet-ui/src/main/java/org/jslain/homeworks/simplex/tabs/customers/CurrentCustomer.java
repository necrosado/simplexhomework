package org.jslain.homeworks.simplex.tabs.customers;

import lombok.Data;
import org.jslain.homeworks.simplex.intranet.backend.dto.CustomerDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.RentalSummaryDto;

import java.util.ArrayList;
import java.util.List;

@Data
public class CurrentCustomer {
    private CustomerDto customerDto = new CustomerDto();
    private List<RentalSummaryDto> rentalSummaryDtos = new ArrayList<>();
}
