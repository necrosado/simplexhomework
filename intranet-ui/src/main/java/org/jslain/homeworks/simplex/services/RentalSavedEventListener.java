package org.jslain.homeworks.simplex.services;

public interface RentalSavedEventListener {

    public void onRentalSaved(Long rentalId);

}
