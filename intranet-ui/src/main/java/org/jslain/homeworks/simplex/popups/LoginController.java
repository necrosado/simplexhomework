/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jslain.homeworks.simplex.popups;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import lombok.Data;
import org.jslain.homeworks.simplex.ApplicationFrame;
import org.jslain.homeworks.simplex.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 *
 * @author ghisl
 */
@Data
@Component
public class LoginController {
    private LoginPopup loginPopup;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    public LoginController(ApplicationFrame jframe) {
        loginPopup = new LoginPopup(jframe, true);
        loginPopup.setLoginController(this);
    }
    
    public void show() {
        loginPopup.getTxtUsername().setText("");
        loginPopup.getTxtPassword().setText("");
        loginPopup.setVisible(true);
    }
    
    public void submit(String username, char[] password){
        SwingUtilities.invokeLater(() -> {
            loginPopup.getLblErrorMsg().setText("");
        });
        loginPopup.repaint();
        SwingUtilities.invokeLater(() -> {
                userService.login(username, password, 
                        (u) -> {
                            loginPopup.setVisible(false);
                        }, 
                        (err) -> {
                            loginPopup.getLblErrorMsg().setText(err);
                        });
            });
    }
    
    public void quit() {
        System.exit(0);
    }
}
