package org.jslain.homeworks.simplex.services;

import lombok.extern.slf4j.Slf4j;
import org.jslain.homeworks.simplex.ApplicationController;
import org.jslain.homeworks.simplex.MessageLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

@Component
@Slf4j
public class ApplicationService {

    @Autowired
    public ApplicationController applicationController;

    private Collection<RentalSavedEventListener> rentalSavedEventListeners = new ArrayList<>();

    public void showErrorMessage(String message) {
        applicationController.setApplicationMessage(message, MessageLevel.ERROR);
    }

    public void showInformationMessage(String message) {
        applicationController.setApplicationMessage(message, MessageLevel.INFO);
    }

    public void showSuccessMessage(String message) {
        applicationController.setApplicationMessage(message, MessageLevel.SUCCESS);
    }

    public void addRentalSavedEventListener(RentalSavedEventListener listener) {
        rentalSavedEventListeners.add(listener);
    }

    public void fireRentalSavedEvent(Long rentalId) {
        log.info("Current rental saved: {}", rentalId);

        for(RentalSavedEventListener listener : rentalSavedEventListeners) {
            listener.onRentalSaved(rentalId);
        }
    }

    public void clearMessage() {
        applicationController.setApplicationMessage("", MessageLevel.INFO);
    }
}
