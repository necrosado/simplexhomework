package org.jslain.homeworks.simplex.tabs.tools;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolInstanceDto;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class ToolsTableModel extends AbstractTableModel implements SaveStatusListenable{

    @Override
    public Status getDataStatus(int row) {
        return Status.UNCHANGED;
    }

    @Data
    @AllArgsConstructor
    private static class ColumnDef {
        String name;
        Class columnClass;
        ToolValueExtractor valueExtractor;
    }

    @FunctionalInterface
    private interface ToolValueExtractor<V> {
        V extract(ToolDto tool);
    }

    private List<ColumnDef> columns = Arrays.asList(
            new ColumnDef("Name", String.class, ToolDto::getToolName),
            new ColumnDef("Quantity", Integer.class, t -> t.getToolInstances().size()),
            new ColumnDef("Quantity available", Long.class,
                    t -> t.getToolInstances()
                            .stream()
                            .filter(ti -> {return ti.getUnavailableSince()
                                    .map(since -> since.isAfter(ZonedDateTime.now()))
                                    .orElse(true);})
                            .count()));

    private List<ToolDto> tools = new ArrayList<>();

    public void setTools(List<ToolDto> tools) {
        this.tools = tools;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return tools.size();
    }

    @Override
    public int getColumnCount() {
        return columns.size();
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columns.get(columnIndex).getName();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columns.get(columnIndex).getColumnClass();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return columns.get(columnIndex).getValueExtractor().extract(tools.get(rowIndex));
    }
}
