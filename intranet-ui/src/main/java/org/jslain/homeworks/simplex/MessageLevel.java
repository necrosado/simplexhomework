package org.jslain.homeworks.simplex;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.awt.*;


@AllArgsConstructor
@Getter
public enum MessageLevel {
    ERROR(Color.RED),
    INFO(Color.BLUE),
    SUCCESS(Color.GREEN);

    private Color color;
}
