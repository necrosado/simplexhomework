package org.jslain.homeworks.simplex.tabs.tools;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import java.awt.*;

@AllArgsConstructor
@Data
public class TableCellStatusDecorator extends DefaultTableCellRenderer {

    private static final Color NEW_FOCUSSED = Color.GREEN;
    private static final Color NEW_UNFOCUSSED = NEW_FOCUSSED.darker();
    private static final Color MODIFIED_FOCUSSED = Color.YELLOW;
    private static final Color MODIFIED_UNFOCUSSED = MODIFIED_FOCUSSED.darker();

    private static Color defaultBgColorFocussed;
    private static Color defaultBgColorUnfocussed;

    private SaveStatusListenable saveStatusListenable;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        SaveStatusListenable.Status status = saveStatusListenable.getDataStatus(row);

        Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if(isSelected && defaultBgColorFocussed == null) {
            defaultBgColorFocussed = component.getBackground();
        }else if (!isSelected && defaultBgColorUnfocussed == null) {
            defaultBgColorUnfocussed = component.getBackground();
        }

        switch (status) {
            case NEW:
                component.setBackground(isSelected ? NEW_FOCUSSED : NEW_UNFOCUSSED);
                break;
            case MODIFIED:
                component.setBackground(isSelected ? MODIFIED_FOCUSSED : MODIFIED_UNFOCUSSED);
                break;
            default:
                component.setBackground(isSelected ? defaultBgColorFocussed : defaultBgColorUnfocussed);
                break;
        }

        return component;
    }
}
