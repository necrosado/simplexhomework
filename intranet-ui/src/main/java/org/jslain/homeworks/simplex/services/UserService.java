/*
 * Copyright (C) 2019 ghisl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.jslain.homeworks.simplex.services;

import org.jslain.homeworks.simplex.intranet.backend.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.BodyExtractors;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 *
 * @author ghisl
 */
@Component
public class UserService {
    
    private List<EventListener<UserChangedEvent>> userChangedListeners = new ArrayList<>();
    private UserChangedEvent lastEvent = new UserChangedEvent(UserChangedEvent.EventType.LOGGED_OUT, null);
    private String cookies;



    private WebClient webClient;

    @Lazy
    public UserService(@Lazy WebClient webClient) {
        this.webClient = webClient;
    }

    public void addUserChangedListener(EventListener<UserChangedEvent> userChangedListener){
        userChangedListeners.add(userChangedListener);
        userChangedListener.onEventFired(lastEvent);
    }
    
    public void removeUserChangedListener(EventListener<UserChangedEvent> userChangedListener){
        userChangedListeners.remove(userChangedListener);
    }
    
    
    public void login(String username, char[] password, final Consumer<UserDto> successCallback, final Consumer<String> errorCallback)
            throws  IllegalArgumentException{

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("username", username);
        map.add("password", new String(password));

        try {
            webClient.post()
                    .uri("/login")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .body(BodyInserters.fromFormData(map))
                    .exchange()
                    .doOnSuccess((cr) -> {
                        cookies = cr.headers().header(HttpHeaders.SET_COOKIE).get(0);

                        webClient.get()
                                .uri("/user")
                                .retrieve()
                                .bodyToMono(UserDto.class)
                                .doOnSuccess((ud) -> {
                                    onLoggedIn(ud, successCallback);
                                })
                                .subscribe();
                    })
            .subscribe();


        }catch(HttpStatusCodeException e){
            onLoginUnsuccessful(e.getMessage(), Optional.ofNullable(errorCallback));
        }
    }

    public void logout() {
        webClient.post()
                .uri("/logout")
                .exchange()
                .doOnSuccess((response) -> {
                    onLoginUnsuccessful("", Optional.empty());
                }).subscribe();
    }

    public String getCookies() {
        return cookies;
    }

    protected void onLoggedIn(UserDto userDto, Consumer<UserDto> successCallback) {


        this.lastEvent = new UserChangedEvent(UserChangedEvent.EventType.LOGGED_IN, userDto);
        userChangedListeners.forEach(
                (ucl) -> ucl.onEventFired(this.lastEvent));
        successCallback.accept(userDto);
    }

    protected void onLoginUnsuccessful(String msg, Optional<Consumer<String>> errorCallback) {
        this.cookies = null;
        this.lastEvent = new UserChangedEvent(UserChangedEvent.EventType.LOGGED_OUT, null);
        userChangedListeners.forEach(
                (ucl) -> ucl.onEventFired(this.lastEvent));

        errorCallback.ifPresent((cb) -> {
            cb.accept(msg);
        });
    }

    /**
     * Any logged in user is, at least, en employee
     * */
    public boolean isEmployee() {
        return lastEvent.getEventType() == UserChangedEvent.EventType.LOGGED_IN;
    }

    /**
     * Any logged in user is, at least, en employee
     * */
    public boolean isAdmin() {
        return isEmployee()
                && lastEvent.getLoggedInUser().getAuthorities().contains("ROLE_ADMIN");
    }
}
