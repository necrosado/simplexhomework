package org.jslain.homeworks.simplex.config;

import org.jslain.homeworks.simplex.ApplicationController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class ProgressBarHttpInterceptor  implements ExchangeFilterFunction {

    @Autowired
    private ApplicationController applicationController;

    private AtomicInteger cmpt = new AtomicInteger(0);

    @Override
    public Mono<ClientResponse> filter(ClientRequest clientRequest, ExchangeFunction next) {
        applicationController.setInProgress(true);
        cmpt.incrementAndGet();

        return next.exchange(clientRequest)
                .flatMap((cr) -> {
                    if(cmpt.decrementAndGet() == 0) {
                        applicationController.setInProgress(false);
                    }
                    return Mono.just(cr);
                });
    }
}
