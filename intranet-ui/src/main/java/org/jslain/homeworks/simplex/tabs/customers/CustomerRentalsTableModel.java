package org.jslain.homeworks.simplex.tabs.customers;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jslain.homeworks.simplex.intranet.backend.dto.CustomerDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.RentalSummaryDto;
import org.jslain.homeworks.simplex.utils.FormatUtils;

import javax.swing.table.AbstractTableModel;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class CustomerRentalsTableModel extends AbstractTableModel {

    private FormatUtils formatUtils = new FormatUtils();

    @Data
    @AllArgsConstructor
    private static class ColumnDef {
        String name;
        Class columnClass;
        CustomerRentalsTableModel.InstanceValueGetter valueExtractor;
    }

    @FunctionalInterface
    private interface InstanceValueGetter<V> {
        V get(RentalSummaryDto rentalSummaryDto);
    }

    private List<CustomerRentalsTableModel.ColumnDef> columnDefs = Arrays.asList(
            new ColumnDef("Start date", String.class, rs -> formatUtils.toDateTime(rs.getStartDate())),
            new ColumnDef("End date", String.class, rs -> formatUtils.toDateTime(rs.getEndDate())),
            new ColumnDef("Tools", String.class, RentalSummaryDto::getTools),
            new ColumnDef("Status", String.class, rsd -> {return rsd.getStatus().getLabel();})
    );

    private List<RentalSummaryDto> rentals = new ArrayList<>();


    @Override
    public int getRowCount() {
        return rentals.size();
    }

    @Override
    public int getColumnCount() {
        return columnDefs.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return columnDefs.get(columnIndex).valueExtractor.get(rentals.get(rowIndex));
    }

    @Override
    public String getColumnName(int column) {
        return columnDefs.get(column).name;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnDefs.get(columnIndex).columnClass;
    }
}
