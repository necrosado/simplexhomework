package org.jslain.homeworks.simplex.services;

import org.jslain.homeworks.simplex.intranet.backend.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Component
public class CustomersService {

    private WebClient webClient;

    public CustomersService(@Lazy WebClient webClient) {
        this.webClient = webClient;
    }

    public List<CustomerDto> search(
            String firstName,
            String lastName,
            String phoneNumber,
            String address
    ) {
        return webClient.post()
                .uri("/customers/search")
                .body(BodyInserters.fromObject(FindCustomersRequestParams.builder()
                        .firstName(firstName)
                        .lastName(lastName)
                        .phoneNumber(phoneNumber)
                        .address(address)
                        .build()))
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<CustomerDto>>(){})
                .block();
    }

    public long save(CustomerDto customerDto) {
        return webClient.post()
                .uri("/customers/save")
                .body(BodyInserters.fromObject(customerDto))
                .retrieve()
                .bodyToMono(Long.class)
                .block();
    }

    public List<RentalSummaryDto> getCustomerRentals(Long id) {
        return webClient.get()
                .uri(uriBuilder -> {
                    return uriBuilder
                        .path("/customers/{id}/rentals")
                        .build(id);})
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<RentalSummaryDto>>() {})
                .block();
    }
}
