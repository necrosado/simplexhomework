package org.jslain.homeworks.simplex.tabs.customers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jslain.homeworks.simplex.intranet.backend.dto.CustomerDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.RentalSummaryDto;
import org.jslain.homeworks.simplex.services.ApplicationService;
import org.jslain.homeworks.simplex.services.CurrentRentalService;
import org.jslain.homeworks.simplex.services.CustomersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomersController {

    @Autowired
    private CustomersService customersService;

    @Autowired
    private CurrentRentalService currentRentalService;

    @Autowired
    private ApplicationService applicationService;

    private CustomersPanel customersPanel;

    private Optional<CurrentCustomer> currentCustomer = Optional.empty();

    public void onInit() {
        this.customersPanel.getTblCustomerSearchResult()
                .getSelectionModel()
                .addListSelectionListener(evt -> {
                    if(!evt.getValueIsAdjusting()) {
                        int index = customersPanel.getTblCustomerSearchResult().getSelectedRow();

                        if(index < 0) {
                            onNoCustomerSelected();
                        } else {
                            onCustomerSelected(index);
                        }
                    }
                });

        this.customersPanel.getTblRentals()
                .getSelectionModel()
                .addListSelectionListener(evt -> {
                    if(!evt.getValueIsAdjusting()) {
                        int index = customersPanel.getTblRentals().getSelectedRow();

                        if(index < 0) {
                            onNoRentalSelected();
                        } else {
                            onRentalSelected(index);
                        }
                    }
                });
    }

    private void onRentalSelected(int index) {
        RentalSummaryDto rentalSummaryDto = customersPanel.getCustomerRentalsTableModel().getRentals().get(index);

        currentRentalService.loadRental(rentalSummaryDto.getId());
    }

    private void onNoRentalSelected() {
    }

    private void onCustomerSelected(int index) {
        CustomerDto customer = customersPanel.getCustomerSearchResultTableModel().getCustomers().get(index);
        CurrentCustomer currentCustomer = new CurrentCustomer();
        currentCustomer.setCustomerDto(customer);
        this.currentCustomer = Optional.of(currentCustomer);
        customersPanel.getBtnSetToCurrentRental().setEnabled(true);
        updateDetailSection();
    }

    private void onNoCustomerSelected() {
        currentCustomer = Optional.empty();
        customersPanel.getBtnSetToCurrentRental().setEnabled(false);
        updateDetailSection();
    }

    void onBtnSearchClicked() {
        if(validateSearchFields()) {
            List<CustomerDto> customers = customersService.search(
                    customersPanel.getTxtFirstNameSearch().getText(),
                    customersPanel.getTxtLastNameSearch().getText(),
                    customersPanel.getTxtPhoneNumberSearch().getText(),
                    customersPanel.getTxtAddressSearch().getText()
            );

            this.applicationService.showInformationMessage(String.format("%d customer(s) found.", customers.size()));

            customersPanel.getCustomerSearchResultTableModel().setCustomers(customers);
            customersPanel.getCustomerSearchResultTableModel().fireTableDataChanged();
        }
    }

    private boolean validateSearchFields() {
        if(StringUtils.isEmpty(customersPanel.getTxtFirstNameSearch().getText())
                && StringUtils.isEmpty(customersPanel.getTxtLastNameSearch().getText())
                && StringUtils.isEmpty(customersPanel.getTxtPhoneNumberSearch().getText())
                && StringUtils.isEmpty(customersPanel.getTxtAddressSearch().getText())) {
            this.applicationService.showErrorMessage("You must at least enter an information for a 'search' to be performed.");
            return false;
        } else {
            this.applicationService.clearMessage();
        }

        return true;
    }

    void onBtnClearSearchClicked() {
        customersPanel.getTxtFirstNameSearch().setText("");
        customersPanel.getTxtLastNameSearch().setText("");
        customersPanel.getTxtPhoneNumberSearch().setText("");
        customersPanel.getTxtAddressSearch().setText("");
    }

    void onBtnNewCustomerClicked() {
        currentCustomer = Optional.of(new CurrentCustomer());
        updateDetailSection();
    }

    void onBtnSaveCustomerClicked() {
        if(currentCustomer.isPresent()) {
            CustomerDto customerDto = currentCustomer.get().getCustomerDto()
                    .toBuilder()
                    .firstName(this.customersPanel.getTxtFirstNameEdit().getText())
                    .lastName(this.customersPanel.getTxtLastNameEdit().getText())
                    .phoneNumber(this.customersPanel.getTxtPhoneNumberEdit().getText())
                    .address(this.customersPanel.getTxtAddressEdit().getText())
                    .build();
            this.customersService.save(customerDto);

            this.applicationService.showInformationMessage("Customer successfully saved.");
        }
    }

    private void updateDetailSection() {
        if(currentCustomer.isPresent()) {
            CurrentCustomer current = currentCustomer.get();
            customersPanel.getTxtFirstNameEdit().setText(current.getCustomerDto().getFirstName());
            customersPanel.getTxtLastNameEdit().setText(current.getCustomerDto().getLastName());
            customersPanel.getTxtPhoneNumberEdit().setText(current.getCustomerDto().getPhoneNumber());
            customersPanel.getTxtAddressEdit().setText(current.getCustomerDto().getAddress());

            customersPanel.getCustomerRentalsTableModel().setRentals(this.customersService.getCustomerRentals(current.getCustomerDto().getId()));
            customersPanel.getCustomerRentalsTableModel().fireTableDataChanged();
        } else {
            customersPanel.getTxtFirstNameEdit().setText("");
            customersPanel.getTxtLastNameEdit().setText("");
            customersPanel.getTxtPhoneNumberEdit().setText("");
            customersPanel.getTxtAddressEdit().setText("");
            customersPanel.getCustomerRentalsTableModel().setRentals(new ArrayList<>());
            customersPanel.getCustomerRentalsTableModel().fireTableDataChanged();
        }
    }

    void onBtnSetToCurrentRental() {
        currentRentalService.changeCustomer(currentCustomer.get().getCustomerDto());
    }
}
