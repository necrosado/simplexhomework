package org.jslain.homeworks.simplex.components;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.net.URI;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author ghisl
 */
@Data
@Slf4j
public class ImagePanel extends JPanel implements ComponentListener {
    private static final String DEFAULT_IMAGE = "/not-applicable.png";

    private URL imageUrl;
    private ImageIcon originalImage;
    private JLabel picLabel;

    private boolean resizing = false;

    public ImagePanel() {
        setLayout(new BorderLayout());
        picLabel = new JLabel();
        picLabel.setName("picLabel");
        picLabel.setMinimumSize(new Dimension(0, 0));
        picLabel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        picLabel.setBorder(new LineBorder(Color.BLACK));
        add(picLabel, BorderLayout.CENTER);

        addComponentListener(this);
        setImageUrl(getClass().getResource(DEFAULT_IMAGE));
    }

    public void setImageUrl(URL url) {
        this.imageUrl = url;
        this.originalImage = null;
        try {
            BufferedImage myPicture = ImageIO.read(imageUrl);
            originalImage = new ImageIcon(myPicture);
        } catch (IOException e) {
            log.error("Cannot load image: " + url);
        }
        this.refreshImage();
    }

    public void setImageData(byte[] data) {
        this.imageUrl = null;
        this.originalImage = null;
        try {
            BufferedImage myPicture = ImageIO.read(new ByteArrayInputStream(data));
            originalImage = new ImageIcon(myPicture);
        } catch (IOException e) {
            log.error("Cannot load image: byte[]");
        }
        this.refreshImage();
    }

    private void refreshImage() {
        if(!resizing){
            resizing = true;

            if(originalImage != null) {
                Image image = originalImage.getImage();
                BigDecimal widthRatio = new BigDecimal(getVisibleRect().width).divide(new BigDecimal(originalImage.getIconWidth()), MathContext.DECIMAL32);
                BigDecimal heightRatio = new BigDecimal(getVisibleRect().height).divide(new BigDecimal(originalImage.getIconHeight()), MathContext.DECIMAL32);
                BigDecimal smallestRatio = widthRatio.min(heightRatio);

                if(!BigDecimal.ZERO.equals(smallestRatio)){
                    int newWidth = smallestRatio.multiply(new BigDecimal(originalImage.getIconWidth())).intValue();
                    int newHeight= smallestRatio.multiply(new BigDecimal(originalImage.getIconHeight())).intValue();

                    if(newWidth > 0 && newHeight > 0) {
                        Image newimg = image.getScaledInstance(
                                newWidth,
                                newHeight,
                                java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
                        ImageIcon imageIcon = new ImageIcon(newimg);  // transform it back
                        picLabel.setIcon(imageIcon);
                    }
                }
                if(getParent() != null){
                    getParent().invalidate();
                }
            }

            resizing = false;
        }

    }

    @Override
    public void setBounds(Rectangle r) {
        super.setBounds(r);
    }

    @Override
    public void setBounds(int x, int y, int width, int height) {
        super.setBounds(x, y, width, height);
    }

    @Override
    public void componentResized(ComponentEvent e) {
        //refreshImage();
    }

    @Override
    public void componentMoved(ComponentEvent e) {}

    @Override
    public void componentShown(ComponentEvent e) {}

    @Override
    public void componentHidden(ComponentEvent e) { }
}
