package org.jslain.homeworks.simplex.services;

import org.jslain.homeworks.simplex.intranet.backend.dto.FindToolsRequestParams;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolImageDto;
import org.jslain.homeworks.simplex.tabs.tools.CurrentTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyExtractors;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.springframework.web.reactive.function.BodyExtractors.toMono;

@Component
public class ToolsService {

    private WebClient webClient;

    public ToolsService(@Lazy WebClient webClient) {
        this.webClient = webClient;
    }

    public List<ToolDto> getTools(String toolName, boolean avaliablesOnly) {

        return webClient.post()
                .uri("/tools/search")
                .body(BodyInserters.fromObject(new FindToolsRequestParams(toolName, avaliablesOnly)))
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<ToolDto>>(){})
                .block();

    }

    public ToolDto getTool(Long toolId) {
        List<ToolDto> tools = new ArrayList<>();

        return webClient.get()
                .uri(uriBuilder -> {
                    return uriBuilder
                            .path("/tools/{id}")
                            .build(toolId);
                })
                .retrieve()
                .bodyToMono(ToolDto.class)
                .block();

    }

    public Long saveTool(CurrentTool toolDto) {
        WebClient.RequestBodyUriSpec request;

        if(toolDto.isNew()){
            request = webClient.post();
        } else {
            request = webClient.put();
        }
        return request
                .uri(uriBuilder -> {
                    return uriBuilder
                            .path("/tools/{id}")
                            .build(toolDto.getTool().getToolId());
                })
                .body(BodyInserters.fromObject(toolDto.getTool()))
                .retrieve()
                .bodyToMono(Long.class)
                .block();
    }

    public Long saveImage(Long toolId, ToolImageDto toolImageDto) {
        WebClient.RequestBodyUriSpec request;

        if(toolImageDto.getIdImage() == null){
            request = webClient.post();
        }else {
            request = webClient.put();
        }

        return request
            .uri(uriBuilder -> {
                return uriBuilder
                        .path("/tools/{id}/images")
                        .build(toolId);
            })
            .body(BodyInserters.fromObject(toolImageDto))
            .retrieve()
            .bodyToMono(Long.class)
            .block();
    }

    public List<ToolImageDto> getToolImages(Long toolId) {
        return webClient.get()
                .uri(uriBuilder -> {
                    return uriBuilder.path("/tools/{id}/images").build(toolId);
                })
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<ToolImageDto>>() {})
                .block();

    }

    public void deleteImage(Long toolId, Long imageId, Consumer<Void> successCallback) {
        webClient.delete()
                .uri(uriBuilder -> {
                    return uriBuilder
                            .path("/tools/{id}/images/{imageId}")
                            .build(toolId, imageId);
                })
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }
}













