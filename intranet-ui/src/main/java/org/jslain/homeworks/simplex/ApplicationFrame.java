/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jslain.homeworks.simplex;

import javax.swing.JFrame;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 *
 * @author ghisl
 */
@Component
@Getter
@Slf4j
public class ApplicationFrame extends javax.swing.JFrame {

    @Getter
    @Setter
    private ApplicationController applicationController;
    
    /**
     * Creates new form ApplicationFrame
     */
    public ApplicationFrame() {
        initComponents();
        this.setExtendedState( this.getExtendedState()|JFrame.MAXIMIZED_BOTH );
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        toolBar = new javax.swing.JToolBar();
        lblErrorMessage = new javax.swing.JLabel();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        progressBar = new javax.swing.JProgressBar();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(10, 0), new java.awt.Dimension(10, 0), new java.awt.Dimension(10, 32767));
        lblUser = new javax.swing.JLabel();
        pnlContainer = new javax.swing.JPanel();
        pnlTabs = new javax.swing.JTabbedPane();
        pnlTools = new org.jslain.homeworks.simplex.tabs.tools.ToolsPanel();
        pnlCustomer = new org.jslain.homeworks.simplex.tabs.customers.CustomersPanel();
        pnlEmployees = new org.jslain.homeworks.simplex.tabs.EmployeesPanel();
        pnlCurrentOrder = new org.jslain.homeworks.simplex.ui.CurrentRentalPanel();
        mainMenuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuItemLogout = new javax.swing.JMenuItem();
        menuItemQuit = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        toolBar.setFloatable(false);
        toolBar.setRollover(true);

        lblErrorMessage.setForeground(new java.awt.Color(255, 0, 0));
        toolBar.add(lblErrorMessage);
        toolBar.add(filler2);

        progressBar.setMaximumSize(new java.awt.Dimension(300, 14));
        toolBar.add(progressBar);
        toolBar.add(filler1);
        toolBar.add(lblUser);

        getContentPane().add(toolBar, java.awt.BorderLayout.SOUTH);

        pnlContainer.setLayout(new java.awt.GridBagLayout());

        pnlTabs.setToolTipText("");
        pnlTabs.setEnabled(false);
        pnlTabs.addTab("Tools", pnlTools);
        pnlTabs.addTab("Customers", pnlCustomer);

        javax.swing.GroupLayout pnlEmployeesLayout = new javax.swing.GroupLayout(pnlEmployees);
        pnlEmployees.setLayout(pnlEmployeesLayout);
        pnlEmployeesLayout.setHorizontalGroup(
            pnlEmployeesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 465, Short.MAX_VALUE)
        );
        pnlEmployeesLayout.setVerticalGroup(
            pnlEmployeesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 666, Short.MAX_VALUE)
        );

        pnlTabs.addTab("Employees", null, pnlEmployees, "Not available yet");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        pnlContainer.add(pnlTabs, gridBagConstraints);

        pnlCurrentOrder.setMinimumSize(new java.awt.Dimension(500, 265));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        pnlContainer.add(pnlCurrentOrder, gridBagConstraints);

        getContentPane().add(pnlContainer, java.awt.BorderLayout.CENTER);

        menuFile.setText("File");

        menuItemLogout.setText("Logout");
        menuItemLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemLogoutActionPerformed(evt);
            }
        });
        menuFile.add(menuItemLogout);

        menuItemQuit.setText("Quit");
        menuItemQuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemQuitActionPerformed(evt);
            }
        });
        menuFile.add(menuItemQuit);

        mainMenuBar.add(menuFile);

        setJMenuBar(mainMenuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menuItemLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemLogoutActionPerformed
        this.applicationController.logout();
    }//GEN-LAST:event_menuItemLogoutActionPerformed

    private void menuItemQuitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemQuitActionPerformed
        this.applicationController.quit();
    }//GEN-LAST:event_menuItemQuitActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ApplicationFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ApplicationFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ApplicationFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ApplicationFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ApplicationFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler2;
    private javax.swing.JLabel lblErrorMessage;
    private javax.swing.JLabel lblUser;
    private javax.swing.JMenuBar mainMenuBar;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenuItem menuItemLogout;
    private javax.swing.JMenuItem menuItemQuit;
    private javax.swing.JPanel pnlContainer;
    private org.jslain.homeworks.simplex.ui.CurrentRentalPanel pnlCurrentOrder;
    private org.jslain.homeworks.simplex.tabs.customers.CustomersPanel pnlCustomer;
    private org.jslain.homeworks.simplex.tabs.EmployeesPanel pnlEmployees;
    private javax.swing.JTabbedPane pnlTabs;
    private org.jslain.homeworks.simplex.tabs.tools.ToolsPanel pnlTools;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JToolBar toolBar;
    // End of variables declaration//GEN-END:variables
}
