package org.jslain.homeworks.simplex.config;

import org.jslain.homeworks.simplex.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class ToolsConfig {

    public static final String REST_TEMPLATE_INTRANET_BACKEND = "rest-template-intranet-backend";

    @Bean(REST_TEMPLATE_INTRANET_BACKEND)
    @Autowired
    public WebClient createRestTemplate(SessionCookieHttpInterceptor sessionCookieHttpInterceptor,
                                        ProgressBarHttpInterceptor progressBarHttpInterceptor,
                                        ForbiddenHttpInterceptor forbiddenHttpInterceptor
    ) {
        WebClient webClient =  WebClient.builder()
                .baseUrl("http://localhost:9094/api")
                .filter(sessionCookieHttpInterceptor)
                .filter(progressBarHttpInterceptor)
                .filter(forbiddenHttpInterceptor)
                .build();

        return webClient;
    }
}
