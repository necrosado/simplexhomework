package org.jslain.homeworks.simplex.services;

public interface EventListener<T> {

    void onEventFired(T event);

}
