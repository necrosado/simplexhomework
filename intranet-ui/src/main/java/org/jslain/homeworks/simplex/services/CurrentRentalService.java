package org.jslain.homeworks.simplex.services;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jslain.homeworks.simplex.intranet.backend.dto.*;
import org.jslain.homeworks.simplex.ui.CurrentRentalController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.jslain.homeworks.simplex.intranet.backend.dto.RentalStatusType.NEW;

@Component
@Slf4j
public class CurrentRentalService {

    private CurrentRentalController currentRentalController;

    private WebClient webClient;

    public CurrentRentalService(@Lazy CurrentRentalController currentRentalController,
                                @Lazy WebClient webClient) {
        this.currentRentalController = currentRentalController;
        this.webClient = webClient;
    }

    public void newRental() {
        currentRentalController.setCustomerDto(Optional.empty());
        currentRentalController.setRentalId(Optional.empty());
        currentRentalController.setStatus(new RentalStatusDto(NEW));
        currentRentalController.setToolInstanceDtoList(Collections.emptyList());
    }

    public void changeCustomer(CustomerDto customerDto) {
        if(currentRentalController.getStatus().getStatus() != NEW) {
            throw new IllegalArgumentException(
                    "Customer can only be changed on NEW rental, not: "
                    + currentRentalController.getStatus().getStatus());
        }

        currentRentalController.setCustomerDto(Optional.of(customerDto));
        currentRentalController.modelToView();
    }

    public void addToolInstance(ToolInstanceDto toolInstanceDto) {
        currentRentalController.addToolInstance(toolInstanceDto);
    }

    public void loadRental(Long idRental) {
        Optional<GetRentalResponseDto> getRentalResponseDto = webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/rentals/{id}")
                        .build(idRental))
                .retrieve()
                .bodyToMono(GetRentalResponseDto.class)
                .blockOptional();

        this.currentRentalController.loadOrder(getRentalResponseDto);
    }

    public boolean isCurrentOrderEditable() {
        return currentRentalController.isEditable();
    }

    public Long save() {
        Long rentalId = null;
        if(!this.currentRentalController.getRentalId().isPresent()) {
            PostRentalRequestDto requestBody = PostRentalRequestDto.builder()
                    .customerId(this.currentRentalController.getCustomerDto().get().getId())
                    .toolInstancesIds(this.currentRentalController.getToolInstanceDtoList()
                            .stream()
                            .map(ToolInstanceDto::getToolInstanceId)
                            .collect(Collectors.toList()))
                    .build();
            rentalId = webClient.post()
                    .uri("/rentals")
                    .body(BodyInserters.fromObject(requestBody))
                    .retrieve()
                    .bodyToMono(Long.class)
                    .block();
        } else {
            log.error("Current rental update NOT SUPPORTED YET");
        }

        return rentalId;
    }

    public void terminate(Long rentalId) {
        webClient.post()
                .uri(uriBuilder -> uriBuilder
                        .path("/rentals/{id}/terminate")
                        .build(rentalId))
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    public void cancel(Long rentalId) {
        webClient.post()
                .uri(uriBuilder -> uriBuilder
                        .path("/rentals/{id}/cancel")
                        .build(rentalId))
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }
}
