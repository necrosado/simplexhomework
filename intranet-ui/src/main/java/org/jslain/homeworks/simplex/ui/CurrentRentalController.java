package org.jslain.homeworks.simplex.ui;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.jdatepicker.DateModel;
import org.jdatepicker.JDateComponentFactory;
import org.jdatepicker.JDatePanel;
import org.jdatepicker.JDatePicker;
import org.jdatepicker.impl.DateComponentFormatter;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.jdatepicker.util.JDatePickerUtil;
import org.jslain.homeworks.simplex.App;
import org.jslain.homeworks.simplex.ApplicationController;
import org.jslain.homeworks.simplex.ApplicationFrame;
import org.jslain.homeworks.simplex.intranet.backend.dto.*;
import org.jslain.homeworks.simplex.services.ApplicationService;
import org.jslain.homeworks.simplex.services.CurrentRentalService;
import org.jslain.homeworks.simplex.services.RentalSavedEventListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;

@Component
@Data
@Slf4j
public class CurrentRentalController implements RentalSavedEventListener {

    private static final String STATUS_NEW = "New";

    @Autowired
    private ApplicationFrame frame;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private CurrentRentalService currentRentalService;

    private CurrentRentalPanel currentRentalPanel;

    private Optional<Long> rentalId = Optional.empty();
    private RentalStatusDto status = new RentalStatusDto(RentalStatusType.NEW);
    private Optional<CustomerDto> customerDto = Optional.empty();
    private List<ToolInstanceDto> toolInstanceDtoList;
    private LocalDate rentalDate = LocalDate.now();

    public boolean isEditable() {
        return status.getStatus() == RentalStatusType.NEW;
    }

    public void onInit() {
        applicationService.addRentalSavedEventListener(this);
        toolInstanceDtoList = this.currentRentalPanel.getToolsTableModel().getToolsInstances();

        currentRentalPanel.getTblTools()
                .getSelectionModel()
                .addListSelectionListener(evt -> {
                    if(!evt.getValueIsAdjusting()){
                        int index = currentRentalPanel.getTblTools().getSelectedRow();
                        if(index < 0) {
                            onNoToolInstanceSelected();
                        }else {
                            onToolInstanceSelected(index);
                        }
                    }
                });

        clearOrder();
        modelToView();
    }

    public void loadOrder(Optional<GetRentalResponseDto> getRentalResponseDto) {
        this.rentalId = Optional.empty();
        clearOrder();

        getRentalResponseDto
                .ifPresent(response -> {
                    this.rentalId = Optional.of(response.getRentalDto().getId());
                    this.status = response.getRentalStatusDto();
                    this.customerDto = Optional.of(response.getRentalDto().getCustomer());
                    this.toolInstanceDtoList.addAll(response.getRentalDto().getToolInstances());
                    this.rentalDate = response.getRentalDto().getStartDate();
                    this.currentRentalPanel.getToolsTableModel().fireTableDataChanged();
                });

        modelToView();
    }

    private void onToolInstanceSelected(int index) {
        currentRentalPanel.getBtnRemoveTool().setEnabled(true);
    }

    private void onNoToolInstanceSelected() {
        currentRentalPanel.getBtnRemoveTool().setEnabled(false);
    }

    private void clearOrder() {
        rentalId = Optional.empty();
        status = new RentalStatusDto(RentalStatusType.NEW);
        customerDto = Optional.empty();
        toolInstanceDtoList.clear();
        rentalDate = LocalDate.now();
        this.currentRentalPanel.getToolsTableModel().fireTableDataChanged();
    }

    public void modelToView() {
        if(isEditable()) {
            ((JComponent)currentRentalPanel.getDatePicker()).setEnabled(true);
        }else {
            ((JComponent)currentRentalPanel.getDatePicker()).setEnabled(false);
        }

        customerDto.ifPresent(c -> {
            currentRentalPanel.getLblFirstNameValue().setText(c.getFirstName());
            currentRentalPanel.getLblLastNameValue1().setText(c.getLastName());
            currentRentalPanel.getLblPhoneNumberValue().setText(c.getPhoneNumber());
            currentRentalPanel.getLblAddressValue().setText(c.getAddress());
        });
        if(!customerDto.isPresent()) {
            currentRentalPanel.getLblFirstNameValue().setText("");
            currentRentalPanel.getLblLastNameValue1().setText("");
            currentRentalPanel.getLblPhoneNumberValue().setText("");
            currentRentalPanel.getLblAddressValue().setText("");
        }

        currentRentalPanel.getLblRentalStatusValue().setText(status.getStatus().getLabel());
        currentRentalPanel.getBtnSaveOrder().setEnabled(this.isReadyToSave());
        currentRentalPanel.getBtnCancel().setEnabled(this.isOrderActive());
        currentRentalPanel.getBtnTerminate().setEnabled(this.isOrderActive());
        currentRentalPanel.getDatePicker().getModel().setDate(rentalDate.getYear(), rentalDate.getMonthValue(), rentalDate.getDayOfMonth());
    }

    private boolean isOrderActive() {
        return this.rentalId.isPresent() && this.status.getStatus() == RentalStatusType.ACTIVE;
    }

    private boolean isReadyToSave() {
        boolean readyToSave = isEditable();

        if(!customerDto.isPresent()){
            readyToSave = false;
        }
        if(currentRentalPanel.getToolsTableModel().getRowCount() == 0) {
            readyToSave = false;
        }

        return readyToSave;
    }

    public void addToolInstance(ToolInstanceDto toolInstanceDto) {
        toolInstanceDtoList.add(toolInstanceDto);
        this.currentRentalPanel.getToolsTableModel().fireTableDataChanged();
        this.modelToView();
    }
    
    void onBtnNewRental() {
        this.clearOrder();
        this.modelToView();
    }

    void onBtnSaveRental() {
        Long rentalId = this.currentRentalService.save();
        applicationService.fireRentalSavedEvent(rentalId);
        applicationService.showSuccessMessage("Order successfully saved");
    }

    public void onRentalSaved(Long rentalId) {
        this.rentalId = Optional.of(rentalId);
        this.currentRentalService.loadRental(rentalId);
    }

    void onDateRentalChanged() {
        DateModel dateModel = currentRentalPanel.getDatePicker().getModel();
        LocalDate date = LocalDate.of(dateModel.getYear(), dateModel.getMonth(), dateModel.getDay());
        this.rentalDate = date;
    }

    public void onBtnRemoveTool() {
        int selectedRow = this.currentRentalPanel.getTblTools().getSelectedRow();
        this.toolInstanceDtoList.remove(selectedRow);
        this.currentRentalPanel.getToolsTableModel().fireTableDataChanged();
    }

    void onBtnCancel() {
        this.currentRentalService.cancel(rentalId.get());
        this.currentRentalService.loadRental(rentalId.get());
    }

    void onBtnTerminate() {
        this.currentRentalService.terminate(rentalId.get());
        this.currentRentalService.loadRental(rentalId.get());
    }
}
