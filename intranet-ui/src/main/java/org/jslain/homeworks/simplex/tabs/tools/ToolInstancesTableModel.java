package org.jslain.homeworks.simplex.tabs.tools;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolInstanceDto;
import org.jslain.homeworks.simplex.utils.FormatUtils;

import javax.swing.table.AbstractTableModel;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

@Getter
public class ToolInstancesTableModel extends AbstractTableModel implements SaveStatusListenable {

    @Setter
    private ToolDto tool = new ToolDto();

    private FormatUtils formatUtis;

    public ToolInstancesTableModel() {
        formatUtis = new FormatUtils();
    }

    @Data
    @AllArgsConstructor
    private static class ColumnDef {
        String name;
        Class columnClass;
        boolean editable;
        InstanceValueGetter valueExtractor;
    }

    @Override
    public Status getDataStatus(int row) {
        Status status = Status.UNCHANGED;

        ToolInstanceDto toolInstanceDto = tool.getToolInstances().get(row);
        if(toolInstanceDto.getToolInstanceId() == null) {
            status = Status.NEW;
        }
        return status;
    }

    @FunctionalInterface
    private interface InstanceValueGetter<V> {
        V get(ToolInstanceDto toolInstance);
    }

    private List<ToolInstancesTableModel.ColumnDef> columns = Arrays.asList(
            new ToolInstancesTableModel.ColumnDef("Name", String.class, true, ToolInstanceDto::getDistinctiveName),
            new ToolInstancesTableModel.ColumnDef("Unavailable since", String.class, false, (ti) -> formatUtis.toDateTime(ti.getUnavailableSince())),
            new ToolInstancesTableModel.ColumnDef("Customer", String.class, false, (ti) -> {return ti.getCustomer().orElse(null);})
            );

    @Override
    public int getRowCount() {
        return tool.getToolInstances().size();
    }

    @Override
    public int getColumnCount() {
        return columns.size();
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columns.get(columnIndex).name;
    }



    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columns.get(columnIndex).columnClass;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columns.get(columnIndex).editable;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ToolInstanceDto toolInstance = tool.getToolInstances().get(rowIndex);
        return columns.get(columnIndex).valueExtractor.get(toolInstance);
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        ToolInstanceDto toolInstance = tool.getToolInstances().get(rowIndex);
        if(columnIndex == 0) {
            toolInstance.setDistinctiveName((String)aValue);
        } else {
            throw new IllegalArgumentException("Only ToolInstanceName is editable");
        }
    }
}
