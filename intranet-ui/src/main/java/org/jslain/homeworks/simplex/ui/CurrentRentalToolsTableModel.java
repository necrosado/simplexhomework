package org.jslain.homeworks.simplex.ui;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolInstanceDto;
import org.jslain.homeworks.simplex.tabs.tools.SaveStatusListenable;
import org.jslain.homeworks.simplex.tabs.tools.ToolInstancesTableModel;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class CurrentRentalToolsTableModel  extends AbstractTableModel implements SaveStatusListenable {

    private static final List<ColumnDef> COLUMNS = Arrays.asList(
            new ColumnDef("Tool", String.class, ToolInstanceDto::getDistinctiveName)
    );

    private List<ToolInstanceDto> toolsInstances = new ArrayList<>();

    @Override
    public Status getDataStatus(int row) {
        return Status.UNCHANGED;
    }

    @Data
    @AllArgsConstructor
    private static class ColumnDef {
        String name;
        Class columnClass;
        CurrentRentalToolsTableModel.InstanceValueGetter valueExtractor;
    }

    @FunctionalInterface
    private interface InstanceValueGetter<V> {
        V get(ToolInstanceDto toolInstance);
    }

    @Override
    public int getRowCount() {
        return toolsInstances.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMNS.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ToolInstanceDto toolInstanceDto = toolsInstances.get(rowIndex);
        return COLUMNS.get(columnIndex).valueExtractor.get(toolInstanceDto);
    }

    @Override
    public String getColumnName(int column) {
        return COLUMNS.get(column).name;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return COLUMNS.get(columnIndex).columnClass;
    }
}
