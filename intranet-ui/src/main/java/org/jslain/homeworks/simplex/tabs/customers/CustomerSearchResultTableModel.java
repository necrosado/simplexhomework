package org.jslain.homeworks.simplex.tabs.customers;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jslain.homeworks.simplex.intranet.backend.dto.CustomerDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolInstanceDto;
import org.jslain.homeworks.simplex.tabs.tools.ToolInstancesTableModel;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class CustomerSearchResultTableModel extends AbstractTableModel {

    @Data
    @AllArgsConstructor
    private static class ColumnDef {
        String name;
        Class columnClass;
        CustomerSearchResultTableModel.InstanceValueGetter valueExtractor;
    }

    @FunctionalInterface
    private interface InstanceValueGetter<V> {
        V get(CustomerDto customerDto);
    }

    private List<ColumnDef> columnDefs = Arrays.asList(
            new ColumnDef("First name", String.class, CustomerDto::getFirstName),
            new ColumnDef("Last name", String.class, CustomerDto::getLastName),
            new ColumnDef("Phone number", String.class, CustomerDto::getPhoneNumber),
            new ColumnDef("Address", String.class, CustomerDto::getAddress)
    );

    private List<CustomerDto> customers = new ArrayList<>();

    @Override
    public int getRowCount() {
        return customers.size();
    }

    @Override
    public int getColumnCount() {
        return columnDefs.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return columnDefs.get(columnIndex).valueExtractor.get(customers.get(rowIndex));
    }

    @Override
    public String getColumnName(int column) {
        return columnDefs.get(column).name;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnDefs.get(columnIndex).columnClass;
    }
}
