package org.jslain.homeworks.simplex;

import jdk.nashorn.internal.objects.annotations.SpecializedFunction;
import org.jslain.homeworks.simplex.popups.LoginPopup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;



/**
 * Hello world!
 *
 */
@SpringBootApplication
public class App 
{    
    public static void main( String[] args )
    {
        SpringApplication application = new SpringApplication(App.class);
        application.setHeadless(false);
        application.setWebApplicationType(WebApplicationType.NONE);
        ApplicationContext appContext = application.run();
        
        ApplicationController applicationController = appContext.getBean(ApplicationController.class);
        
        applicationController.show();
    }
}
