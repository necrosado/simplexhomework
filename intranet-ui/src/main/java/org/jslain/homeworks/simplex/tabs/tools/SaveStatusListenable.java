package org.jslain.homeworks.simplex.tabs.tools;

public interface SaveStatusListenable {
    enum Status {
        UNCHANGED,
        NEW,
        MODIFIED;
    }

    Status getDataStatus(int row);
}