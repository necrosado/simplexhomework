package org.jslain.homeworks.simplex.config;

import org.jslain.homeworks.simplex.ApplicationController;
import org.jslain.homeworks.simplex.ApplicationFrame;
import org.jslain.homeworks.simplex.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import reactor.core.publisher.Mono;

import javax.swing.*;
import java.io.IOException;

@Component
public class SessionCookieHttpInterceptor implements ExchangeFilterFunction {

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationFrame frame;

    @Override
    public Mono<ClientResponse> filter(ClientRequest clientRequest, ExchangeFunction next) {
        String cookies = userService.getCookies();
        if(cookies != null) {
            clientRequest = ClientRequest.from(clientRequest)
                    .header(HttpHeaders.COOKIE, cookies)
                    .build();
        }
        return next.exchange(clientRequest)
                .doOnError(HttpClientErrorException.class, (err) -> {
                    if(err.getStatusCode() == HttpStatus.FORBIDDEN){
                        userService.logout();
                        JOptionPane.showMessageDialog(
                                frame,
                                "Your session is not valid, or you tried an authorised operation."
                        );
                    }
                });
    }
}
