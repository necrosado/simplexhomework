package org.jslain.homeworks.simplex.config;

public interface SessionExpiredAware {
    void onSessionExpired();
}
