/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jslain.homeworks.simplex;

import lombok.extern.slf4j.Slf4j;
import org.jslain.homeworks.simplex.config.SessionExpiredAware;
import org.jslain.homeworks.simplex.popups.LoginController;
import org.jslain.homeworks.simplex.services.UserChangedEvent;
import org.jslain.homeworks.simplex.services.UserService;
import org.jslain.homeworks.simplex.tabs.customers.CustomersController;
import org.jslain.homeworks.simplex.ui.CurrentRentalController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import org.jslain.homeworks.simplex.tabs.tools.ToolsController;

/**
 *
 * @author ghisl
 */
@Slf4j
@Component
public class ApplicationController implements SessionExpiredAware {

    private static final String NOT_LOGGED_USER_LABEL = "Not logged yet.";
    
    @Autowired
    private ApplicationFrame frame;
    
    @Autowired
    private LoginController loginController;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private ToolsController toolsController;

    @Autowired
    private CustomersController customersController;

    @Autowired
    private CurrentRentalController currentOrderController;

    @PostConstruct
    public void onInit() {
        this.frame.setApplicationController(this);

        this.toolsController.setToolsPanel(this.frame.getPnlTools());
        this.frame.getPnlTools().setToolsController(this.toolsController);
        this.toolsController.onInit();

        this.customersController.setCustomersPanel(this.frame.getPnlCustomer());
        this.frame.getPnlCustomer().setCustomersController(this.customersController);
        this.customersController.onInit();

        this.currentOrderController.setCurrentRentalPanel(frame.getPnlCurrentOrder());
        frame.getPnlCurrentOrder().setController(this.currentOrderController);
        this.currentOrderController.onInit();

        this.frame.getLblUser().setText(NOT_LOGGED_USER_LABEL);
        this.frame.getProgressBar().setVisible(false);


    }
    
    public void show() {
        frame.setVisible(true);
        
        userService.addUserChangedListener((evt) -> {
            switch(evt.getEventType()){
                case LOGGED_OUT:
                    onLoggedOut();
                    break;
                case LOGGED_IN:
                    onLoggedIn(evt);
                    break;
            }
        });
    }

    public void logout() {
        int response = JOptionPane.showConfirmDialog(frame, "Are you sure you want to log out?");
        if(response == JOptionPane.OK_OPTION){
            this.userService.logout();
        }
    }

    public void quit() {
        int response = JOptionPane.showConfirmDialog(frame, "Are you sure you want to close this application?");
        if(response == JOptionPane.OK_OPTION){
            this.userService.logout();
            System.exit(0);
        }
    }
    
    private void onLoggedOut() {
        this.frame.getLblUser().setText(NOT_LOGGED_USER_LABEL);
        frame.getPnlTabs().setEnabled(false);
        frame.getPnlTabs().setEnabledAt(0, false);

        loginController.show();
    }
    
    private void onLoggedIn(UserChangedEvent evt) {
        frame.getLblUser().setText(
                String.format("%s %s",
                        evt.getLoggedInUser().getFirstName(),
                        evt.getLoggedInUser().getLastName()));

        frame.getPnlTabs().setEnabled(true);
        frame.getPnlTabs().setEnabledAt(0, this.userService.isEmployee());

    }

    public void setInProgress(boolean inProgress) {
        SwingUtilities.invokeLater(() -> {
            frame.getProgressBar().setIndeterminate(inProgress);
            frame.getProgressBar().setVisible(inProgress);
        });
    }

    @Override
    public void onSessionExpired() {
        log.info("Session expired! " + this.getClass().getSimpleName());
    }

    public void setApplicationMessage(String message, MessageLevel level) {
        this.frame.getLblErrorMessage().setForeground(level.getColor());
        this.frame.getLblErrorMessage().setText(message);
    }
}
