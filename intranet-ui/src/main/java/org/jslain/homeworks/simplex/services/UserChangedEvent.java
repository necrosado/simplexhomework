/*
 * Copyright (C) 2019 ghisl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.jslain.homeworks.simplex.services;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jslain.homeworks.simplex.intranet.backend.dto.UserDto;

/**
 *
 * @author ghisl
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserChangedEvent {
    public static enum EventType {
        LOGGED_IN,
        LOGGED_OUT;
    }
    
    private EventType eventType;
    private UserDto loggedInUser;
}
