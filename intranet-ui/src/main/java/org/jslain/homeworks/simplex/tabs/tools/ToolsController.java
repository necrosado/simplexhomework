package org.jslain.homeworks.simplex.tabs.tools;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.jslain.homeworks.simplex.config.SessionExpiredAware;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolImageDto;
import org.jslain.homeworks.simplex.intranet.backend.dto.ToolInstanceDto;
import org.jslain.homeworks.simplex.services.CurrentRentalService;
import org.jslain.homeworks.simplex.services.ToolsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.FileSystemUtils;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.html.Option;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author ghisl
 */
@Component
@Data
@Slf4j
public class ToolsController implements SessionExpiredAware {

    private static final int NO_INSTANCE_SELECTED = -1;

    @Autowired
    private ToolsService toolsService;

    @Autowired
    private CurrentRentalService currentRentalService;

    private ToolsPanel toolsPanel;

    private Optional<CurrentTool> selectedTool = Optional.empty();

    private int selectedToolInstanceIndex = NO_INSTANCE_SELECTED;

    public void onInit() {
        toolsPanel.getTblResults()
                .getSelectionModel()
                .addListSelectionListener(evt -> {
                    if(!evt.getValueIsAdjusting()){
                        int index = toolsPanel.getTblResults().getSelectedRow();
                        if(index < 0) {
                            onNoToolSelected();
                        }else {
                            onToolSelected(index);
                        }
                        updateDetailSection();
                    }
                });

        toolsPanel.getTblInstances()
                .getSelectionModel()
                .addListSelectionListener(evt -> {
                    if(!evt.getValueIsAdjusting()){
                        int index = toolsPanel.getTblInstances().getSelectedRow();
                        if(index < 0) {
                            onNoToolInstanceSelected();
                        }else {
                            onToolInstanceSelected(index);
                        }
                    }
                });

        updateDetailSection();
    }
    
    private void onToolSelected(int index) {
        selectedTool = Optional.of(new CurrentTool(toolsPanel.getToolsTableModel().getTools().get(index)));
    }
    
    private void onNoToolSelected() {
        selectedTool = Optional.empty();
    }

    private void onToolInstanceSelected(int index) {
        this.selectedToolInstanceIndex = index;
        toolsPanel.getBtnRemoveInstance().setEnabled(true);
        ToolInstanceDto toolInstance = toolsPanel.getToolInstancesTableModel().getTool().getToolInstances().get(this.selectedToolInstanceIndex);
        toolsPanel.getBtnAddToCurrentOrder().setEnabled(
                currentRentalService.isCurrentOrderEditable()
                && !toolInstance.getUnavailableSince().isPresent());
    }

    private void onNoToolInstanceSelected() {
        this.selectedToolInstanceIndex = NO_INSTANCE_SELECTED;
        toolsPanel.getBtnRemoveInstance().setEnabled(false);
        toolsPanel.getBtnAddToCurrentOrder().setEnabled(false);
    }

    private void updateDetailSection() {
        if(selectedTool.isPresent()){
            fromModelToView();
        } else {
            clearDetailFields();
        }
    }

    private void clearDetailFields() {
        toolsPanel.getPnlImage().setImageUrl(
                ToolsController.class.getResource("/not-applicable.png"));
        toolsPanel.getTxtToolName().setText("");
        toolsPanel.getTxtDescription().setText("");
        toolsPanel.getSpinnerToolPrice().setValue(0.00d);
        toolsPanel.getToolInstancesTableModel().setTool(new ToolDto());
        toolsPanel.getToolInstancesTableModel().fireTableDataChanged();
        toolsPanel.getBtnSetImage().setEnabled(false);
        toolsPanel.getBtnRemoveImage().setEnabled(false);
        toolsPanel.getBtnAddInstance().setEnabled(false);
        toolsPanel.getBtnRemoveInstance().setEnabled(false);
        toolsPanel.getBtnAddToCurrentOrder().setEnabled(false);
        toolsPanel.getBtnSave().setEnabled(false);
    }

    private void fromModelToView() {
        selectedTool.ifPresent(st -> {
            if(st.getTool().getImage().isPresent()) {
                toolsPanel.getPnlImage().setImageData(st.getTool().getImage().get().getData());
            }else {
                toolsPanel.getPnlImage().setImageUrl(
                        ToolsController.class.getResource("/not-applicable.png"));
            }
            toolsPanel.getTxtToolName().setText(st.getTool().getToolName());
            toolsPanel.getTxtDescription().setText(st.getTool().getToolDescription());
            toolsPanel.getSpinnerToolPrice().setValue(st.getTool().getPrice().doubleValue());
            toolsPanel.getToolInstancesTableModel().setTool(st.getTool());
            toolsPanel.getToolInstancesTableModel().fireTableDataChanged();

            if(st.getTool().getImage().isPresent()) {
                toolsPanel.getBtnSetImage().setEnabled(false);
                toolsPanel.getBtnRemoveImage().setEnabled(true);
            } else {
                toolsPanel.getBtnSetImage().setEnabled(true);
                toolsPanel.getBtnRemoveImage().setEnabled(false);
            }

            toolsPanel.getBtnAddInstance().setEnabled(true);
            toolsPanel.getBtnSave().setEnabled(true);
        });

    }

    private void fromViewToModel() {
        selectedTool.ifPresent(st -> {
            st.getTool().setToolName(toolsPanel.getTxtToolName().getText());
            st.getTool().setToolDescription(toolsPanel.getTxtDescription().getText());
            st.getTool().setPrice(new BigDecimal((Double)toolsPanel.getSpinnerToolPrice().getValue()));
        });
    }

    public void onSearch() {
        toolsPanel.getToolsTableModel().setTools(
            toolsService.getTools(
                    toolsPanel.getTxtSearchToolName().getText(),
                    toolsPanel.getChkAvailableOnly().isSelected()));
    }

    public void onClearSearchFields() {
        this.toolsPanel.getTxtSearchToolName().setText("");
        toolsPanel.getToolsTableModel().getTools().clear();
        toolsPanel.getToolsTableModel().fireTableDataChanged();
    }

    public void onNewToolClick() {
        log.info("new tool");
        selectedTool = Optional.of(new CurrentTool());
        updateDetailSection();
    }

    public void onDeleteToolClick() {
        log.info("delete tools");
    }

    public void onSave() {
        fromViewToModel();

        Long idTool = toolsService.saveTool(selectedTool.get());

        ToolDto tool = toolsService.getTool(idTool);
        this.selectedTool = Optional.of(new CurrentTool(tool));
        fromModelToView();
    }

    void onAddImage() {
        JFileChooser jfc = new JFileChooser();
        String[] imgExtensions = ImageIO.getReaderFileSuffixes();
        jfc.setFileFilter(new FileNameExtensionFilter(
                String.format("Images (%s)", Stream.of(imgExtensions).collect(Collectors.joining(", "))),
                imgExtensions));
        if(jfc.showOpenDialog(toolsPanel) == JFileChooser.APPROVE_OPTION) {
            File selectedImage = jfc.getSelectedFile();
            log.info("Selected image: {}", selectedImage.getName());

            ToolImageDto toolImageDto = null;
            try {
                toolImageDto = ToolImageDto.builder()
                        .filename(selectedImage.getName())
                        .data(Files.readAllBytes(selectedImage.toPath()))
                        .build();
            } catch (IOException e) {
                e.printStackTrace();
            }

            this.selectedTool.get().getTool().setImage(Optional.ofNullable(toolImageDto));
            this.fromModelToView();
        }
    }

    void onRemoveImage() {
        this.selectedTool.get().getTool().setImage(Optional.empty());
        this.fromModelToView();
    }

    @Override
    public void onSessionExpired() {
        log.info("Session expired! " + this.getClass().getSimpleName());
    }

    void onAddToolInstance() {
        this.selectedTool.get().getTool().getToolInstances().add(new ToolInstanceDto());
        this.fromModelToView();
    }

    void onRemoveToolInstance() {
        log.error("onRemoveToolInstance()");

    }

    void onAddToCurrentOrder() {
        ToolInstanceDto toolInstanceDto = selectedTool.get().getTool().getToolInstances().get(this.selectedToolInstanceIndex);
        this.currentRentalService.addToolInstance(toolInstanceDto);
    }


}
