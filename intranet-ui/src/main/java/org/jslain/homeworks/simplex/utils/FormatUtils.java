package org.jslain.homeworks.simplex.utils;

import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Component
public class FormatUtils {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public String toDateTime(Optional<ZonedDateTime> zdt) {
        return zdt.map(it -> it.format(DATE_TIME_FORMATTER)).orElse(null);
    }
}
