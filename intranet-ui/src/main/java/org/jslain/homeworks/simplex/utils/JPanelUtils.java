package org.jslain.homeworks.simplex.utils;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class JPanelUtils {

    public static void setDefaultButton(JPanel panel, JButton defaultButton) {

        ActionMap actionMap = panel.getActionMap();
        int condition = JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT;
        InputMap inputMap = panel.getInputMap(condition );


        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "VK_ENTER");

        actionMap.put("VK_ENTER", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                defaultButton.doClick();
            }
        });
    }
}
