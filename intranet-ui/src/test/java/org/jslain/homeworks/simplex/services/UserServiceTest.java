package org.jslain.homeworks.simplex.services;

import org.jslain.homeworks.simplex.SpringTestConfig;
import org.jslain.homeworks.simplex.intranet.backend.dto.UserDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.response.MockRestResponseCreators;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.assertThat;
import static org.jslain.homeworks.simplex.services.UserChangedEvent.EventType.LOGGED_IN;
import static org.jslain.homeworks.simplex.services.UserChangedEvent.EventType.LOGGED_OUT;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;

/*
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserServiceTest {
    MockRestServiceServer mockServer;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    UserService underTest;

    @Before
    public void setup() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void whenAddUserChangedListener_givenBeforeLogin_thenNotLoggedEventFired() {
        underTest.addUserChangedListener((evt) -> {
            assertThat(evt.getEventType()).isEqualTo(LOGGED_OUT);
        });
    }

    @Test
    public void whenAddUserChangedListener_givenOnceLoggedIn_thenLoggedEventFired() {
        underTest.onLoggedIn(null, (u) -> {});

        underTest.addUserChangedListener((evt) -> {
            assertThat(evt.getEventType()).isEqualTo(LOGGED_IN);
        });
    }

    @Test
    public void whenAddUserChangedListener_givenAddedBeforeLogin_thenLoggedEventFired() {
        AtomicInteger cmpt = new AtomicInteger();
        underTest.addUserChangedListener((evt) -> {
            if(cmpt.getAndIncrement() == 0) {
                assertThat(evt.getEventType()).isEqualTo(LOGGED_OUT);
            }else{
                assertThat(evt.getEventType()).isEqualTo(LOGGED_IN);
            }
        });

        underTest.onLoggedIn(new UserDto(), (u)->{});
    }

    @Test
    public void whenRemoveUserChangedListener_thenListenerNotCalledAnymoreOnNewEvents() {
        EventListener<UserChangedEvent> mockListener = mock(EventListener.class);
        underTest.addUserChangedListener(mockListener);
        verify(mockListener, times(1)).onEventFired(any());
        mockServer.expect(ExpectedCount.once(),
                requestTo("/login"))
                .andExpect(method(POST))
                .andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK));

        underTest.removeUserChangedListener(mockListener);
        underTest.login("foo", "bar".toCharArray(), (u)->{}, (err)->{});

        verifyNoMoreInteractions(mockListener);
    }

    @Test
    public void whenLogin_givenForbidden_thenErrorWithMessage() {
        mockServer.expect(ExpectedCount.once(),
                requestTo("/login"))
        .andExpect(method(POST))
        .andRespond(MockRestResponseCreators.withStatus(HttpStatus.FORBIDDEN));

        underTest.login("foo", "bar".toCharArray(), (u)->{}, (err)->{});

        mockServer.verify();
    }


}
 */