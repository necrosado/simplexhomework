package org.jslain.homeworks.simplex;

import org.jslain.homeworks.simplex.services.UserService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import javax.xml.ws.WebServiceClient;

@Configuration
public class SpringTestConfig {

    @Bean
    @Scope()
    public RestTemplate createRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public WebClient getWebClient() {
        return Mockito.mock(WebClient.class);
    }

    @Bean
    public UserService getUserService() {
        return new UserService(getWebClient());
    }
}
