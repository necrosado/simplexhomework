import { Component, OnInit } from '@angular/core';
import { ToolsService } from 'src/app/services/tools.service';
import { AvailableTool } from 'src/app/dto/available.tool';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  availableTools : AvailableTool[] = [];

  constructor(private toolsService: ToolsService) { }

  ngOnInit() {
    this.toolsService.getAvailableTools().subscribe(at => {
      this.availableTools = at;
    });
  }

}
