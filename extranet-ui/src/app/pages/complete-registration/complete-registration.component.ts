import { Component, OnInit, NgZone } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { formGroupNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/session.service';

@Component({
  selector: 'app-complete-registration',
  templateUrl: './complete-registration.component.html',
  styleUrls: ['./complete-registration.component.css']
})
export class CompleteRegistrationComponent implements OnInit {

  formGroup: FormGroup;

  constructor(private fb: FormBuilder,
    private httpClient: HttpClient,
    private router: Router,
    private zone: NgZone,
    private sessionService: SessionService) { }
    
  ngOnInit() {
    this.formGroup = this.fb.group({
      'firstName': this.fb.control(null, Validators.required),
      'lastName': this.fb.control(null, Validators.required),
      'phoneNumber': this.fb.control(null, [Validators.required, this.phoneNumberValidator()]),
      'address': this.fb.control(null, Validators.required)
    });
  }

  private phoneNumberValidator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
      if(control.value){
        const valueStr : string = control.value;
        const value = valueStr.replace(/[^0-9]/g, '');
  
        return /^[0-9]{10}$/.test(value) ? null : {'pattern': {value: control.value}};
      }
      return null;
    };
  }

  get firstName() {
    return this.formGroup.get('firstName');
  }

  get lastName() {
    return this.formGroup.get('lastName');
  }

  get phoneNumber() {
    return this.formGroup.get('phoneNumber');
  }

  get address() {
    return this.formGroup.get('address');
  }

  onSubmit() {
    this.httpClient.post("/api/users/complete-registration", this.formGroup.value)
      .subscribe(() => {
        this.sessionService.refreshCurrentUser();
        this.zone.run(() => {
          this.router.navigate(['/']);
        });
      });
  }
}
