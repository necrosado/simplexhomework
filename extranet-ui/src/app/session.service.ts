import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReplaySubject, Observable } from 'rxjs';
import { Customer } from './dto/customer';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  constructor(
    private httpClient: HttpClient) { }

  public currentUser = new ReplaySubject<Customer>(1);

  public logout() {
    return this.httpClient.post('/api/logout', {responseType: 'text'})
      .subscribe(() => {
        this.currentUser.next(null);
      },
      (err) => {
        this.currentUser.next(null);
      });
  }

  public refreshCurrentUser() {
    return this.httpClient.get<Customer>('/api/users/current')
      .subscribe(currentUser => {
        this.currentUser.next(currentUser);
      },
      (err) => {
        this.currentUser.next(null);
      });
  }
}
