import { async, ComponentFixture, TestBed, getTestBed, tick, fakeAsync } from '@angular/core/testing';

import { PostLoginComponent } from './post-login.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SessionService } from '../session.service';
import { Session } from 'inspector';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';

import { routes } from '../app-routing.module'
import { HomepageComponent } from '../pages/homepage/homepage.component';
import { CompleteRegistrationComponent } from '../pages/complete-registration/complete-registration.component';
import { LoginComponent } from '../pages/login/login.component';
import { Location } from '@angular/common';
import { Customer } from '../dto/customer';
import { ReactiveFormsModule } from '@angular/forms';

describe('PostLoginComponent', () => {
let USER : Customer = {
  address: '',
  firstName: '',
  lastName: '',
  phoneNumber: '',
  username: ''
};

  let component: PostLoginComponent;
  let fixture: ComponentFixture<PostLoginComponent>;
  let sessionService: SessionService;
  let router: Router;
  let location: Location;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [ 
        PostLoginComponent,
        HomepageComponent,
        CompleteRegistrationComponent,
        LoginComponent
      ],
      imports: [
        RouterTestingModule.withRoutes(routes),
        HttpClientTestingModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();

    sessionService = getTestBed().get(SessionService);
    router = getTestBed().get(Router);
    location = getTestBed().get(Location);

    spyOn(sessionService, 'refreshCurrentUser');
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an untouched location', () => {
    expect(location.path()).toBe('');
  });

  describe('onInit', () => {
    it('should redirect when current user is defined', fakeAsync(() => {
      sessionService.currentUser.next(USER);
      tick();

      expect(location.path()).toBe('/home');
    }));

    it('should do nothing when currentUser is null', fakeAsync(() => {
      sessionService.currentUser.next(null);
      tick();

      expect(location.path()).toBe('');
    }));

    it('should call refreshCurrentUser', () => {
      expect(sessionService.refreshCurrentUser).toHaveBeenCalled();
    });
  });
  
});
