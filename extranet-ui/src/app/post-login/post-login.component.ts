import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SessionService } from '../session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-login',
  templateUrl: './post-login.component.html',
  styleUrls: ['./post-login.component.css']
})
export class PostLoginComponent implements OnInit {

  constructor(private sessionService: SessionService,
    private router: Router) { }

  ngOnInit() {
    this.sessionService.currentUser.subscribe((user) => {
      if(user){
        if(user.firstName) {
          this.router.navigate(['/']);
        } else {
          this.router.navigate(['/complete-registration']);
        }
      }
    });
    this.sessionService.refreshCurrentUser();
  }

}
