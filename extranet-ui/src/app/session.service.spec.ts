import { TestBed, getTestBed } from '@angular/core/testing';

import { SessionService } from './session.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Customer } from './dto/customer';

describe('SessionService', () => {
  let USER : Customer = {
    address: '123',
    firstName: 'Foo',
    lastName: 'Bar',
    phoneNumber: '888-555-1212',
    username: 'Foo Bar'
  };
  let httpTesting: HttpTestingController;
  let service : SessionService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });

    httpTesting = getTestBed().get(HttpTestingController);
    service = TestBed.get(SessionService);
  });

  afterEach(() => {
    httpTesting.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('logout()', () => {
    it('should call /logout rest service', () => {
      service.logout();

      const req = httpTesting.expectOne('/logout');
    });

    it('should push an empty "currentUser" once logged out', () => {
      let currentUser = USER;
      service.currentUser.next(USER);
      service.logout();

      const req = httpTesting.expectOne('/logout');
      req.flush({});

      service.currentUser.subscribe((user) => {
        currentUser = user;
      });
      expect(currentUser).toBeNull();
    });
  });

  describe('refreshCurrentUser()', () => {
    it('should call /users/current rest endpoint', () => {
      service.refreshCurrentUser();

      const req = httpTesting.expectOne('/users/current');
      expect(req.request.method).toBe('GET');
    });

    it('should publish new user on success', () => {
      let currentUser = null;
      service.currentUser.next(null);
      service.refreshCurrentUser();

      const req = httpTesting.expectOne('/users/current');
      req.flush(USER);

      service.currentUser.subscribe((user) => {
        currentUser = user;
      });
      expect(currentUser).toBe(USER);
    });

    it('should publish empty user on error', () => {
      let currentUser = USER;
      service.currentUser.next(USER);
      service.refreshCurrentUser();

      const req = httpTesting.expectOne('/users/current');
      req.flush(USER, {
        status: 400,
        statusText: 'An error occured'
      });

      service.currentUser.subscribe((user) => {
        currentUser = user;
      });
      expect(currentUser).toBeNull();
    })
  });
});