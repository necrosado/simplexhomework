import { TestBed, async, getTestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HttpClient } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { SessionService } from './session.service';
import { of } from 'rxjs';
import { Customer } from './dto/customer';
import { DebugElement } from '@angular/core';
import { RouterLinkWithHref } from '@angular/router';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {
  let injector: TestBed;
  let httpTesting: HttpTestingController;
  let sessionService: SessionService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ]
    }).compileComponents();

    injector = getTestBed();
    httpTesting = injector.get(HttpTestingController);
    sessionService = injector.get(SessionService);

    spyOn(sessionService, 'logout');
    spyOn(sessionService, 'refreshCurrentUser');
  }));


  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should refresh the current user', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    
    expect(sessionService.refreshCurrentUser).toHaveBeenCalled();
  });

  it(`should have as title 'client'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('client');
  });

  describe('when not logged in', () => {
    it('should display menu items "Home" and "Login"', () => {
      const fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
      
      expect(fixture.debugElement.query(By.css('#menuHomeLink'))).toBeTruthy();
      expect(fixture.debugElement.query(By.css('#menuLoginLink'))).toBeTruthy();
      expect(fixture.debugElement.query(By.css('#menuUsername'))).toBeFalsy();
      expect(fixture.debugElement.query(By.css('#menuLogoutLink'))).toBeFalsy();
    });
  });

  describe('when logged in', () => {
    beforeEach(() => {
      sessionService.currentUser.next({
        username: 'Foo Bar',
        address:'',
        firstName:'',
        lastName:'',
        phoneNumber:''
      });
    });

    it('should display menu items "Home", the user name, and "Logout" link', () => {
      const fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
      
      expect(fixture.debugElement.query(By.css('#menuHomeLink'))).toBeTruthy();
      expect(fixture.debugElement.query(By.css('#menuLoginLink'))).toBeFalsy();
      expect(fixture.debugElement.query(By.css('#menuUsername'))).toBeTruthy();
      expect(fixture.debugElement.query(By.css('#menuLogoutLink'))).toBeTruthy();
    });
  });

  describe('logout()', () => {
    it('should call sessionService.logout()', async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
  
      fixture.componentInstance.logout();
      
      expect(sessionService.logout).toHaveBeenCalled();
    }));
  });

  describe('', () => {

  });
});
