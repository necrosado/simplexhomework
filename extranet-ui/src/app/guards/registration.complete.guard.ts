import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { SessionService } from '../session.service';
/*import { currentId } from 'async_hooks';*/

export class RegistrationCompleteGuard implements CanActivate {
    
    private loggedIn = false;
    private registrationCompleted = false;

    constructor(
        private sessionService: SessionService,
        private router: Router) {
        this.sessionService.currentUser.subscribe(currentUser => {
            this.loggedIn = currentUser != null;
            this.registrationCompleted = this.loggedIn && currentUser.firstName != null;
        });
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if(!this.loggedIn){
            this.router.navigate(['/login']);
        } else if(!this.registrationCompleted){
            this.router.navigate(['/complete-registration']);
        }

        return this.registrationCompleted;
    }
}