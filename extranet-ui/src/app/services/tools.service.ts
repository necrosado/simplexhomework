import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AvailableTool } from '../dto/available.tool';

@Injectable({
  providedIn: 'root'
})
export class ToolsService {

  constructor(private httpClient: HttpClient) { }

  getAvailableTools() {
    return this.httpClient.get<AvailableTool[]>('/api/public/tools', )
  }
}
