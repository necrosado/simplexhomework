export class AvailableTool{
    name: string;
    description: string;
    price: number;
    numberAvailable: number;
}