export class Customer {
    firstName: string;
    lastName: string;
    phoneNumber: string;
    address: string;
}