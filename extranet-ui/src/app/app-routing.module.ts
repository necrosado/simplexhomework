import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { LoginComponent } from './pages/login/login.component';
import { PostLoginComponent } from './post-login/post-login.component';
import { CompleteRegistrationComponent } from './pages/complete-registration/complete-registration.component';

export const routes: Routes = [
  {path: 'home', component: HomepageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'complete-registration', component: CompleteRegistrationComponent},
  {path: 'post-login', component: PostLoginComponent},
  {path: '', pathMatch:'full', redirectTo: 'home'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
