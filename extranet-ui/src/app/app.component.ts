import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from './session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'client';

  loggedIn = false;
  currentUser;

  constructor(private sessionService: SessionService,
    private router: Router) {}

  ngOnInit() {
    this.sessionService.currentUser.subscribe(currentUser => {
      this.loggedIn = !!currentUser;
      this.currentUser = currentUser;
    });

    this.sessionService.refreshCurrentUser();
  }

  public logout() {
    this.sessionService.logout();
  }
}
