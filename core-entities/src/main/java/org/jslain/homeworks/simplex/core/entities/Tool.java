package org.jslain.homeworks.simplex.core.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity()
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Tool {

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long idTool;

  @Column
  private String name;

  @Column
  private String nameSearch;

  @Column
  private String description;

  @Column
  private String descriptionSearch;

  @Column
  private BigDecimal price;

  @OneToMany(mappedBy = "tool", targetEntity = Image.class)
  private List<Image> imagesIds;


  @OneToMany(mappedBy = "tool")
  private List<ToolInstance> toolInstances;
}
