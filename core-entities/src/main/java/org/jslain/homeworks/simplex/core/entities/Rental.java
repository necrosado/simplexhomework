package org.jslain.homeworks.simplex.core.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
@Data
public class Rental {

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long idRental;

  @ManyToOne
  @JoinColumn(name = "id_customer")
  private Customer customer;

  @OneToMany(mappedBy = "rental", cascade = {CascadeType.ALL}, orphanRemoval = true)
  private List<RentalItem> rentalItems = new ArrayList<>();

  @OneToMany(mappedBy = "rental", cascade = {CascadeType.ALL}, orphanRemoval = true)
  private List<RentalStatus> rentalStatuses = new ArrayList<>();
}
