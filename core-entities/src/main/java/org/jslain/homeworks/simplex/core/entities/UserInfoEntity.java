package org.jslain.homeworks.simplex.core.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserInfoEntity {

  @Id
  @Column(name = "username")
  private String username;

  @Column
  private String firstName;

  @Column
  private String lastName;

  @Column
  private String phoneNumber;

  @Column
  private String address;
}
