package org.jslain.homeworks.simplex.core.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class SearchUtils {

    public String toSearchString(String str) {
        return StringUtils.stripAccents(str)
                .toUpperCase()
                .replaceAll("[^A-Z0-9]", "");
    }
}
