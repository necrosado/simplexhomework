package org.jslain.homeworks.simplex.core.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Table
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RentalStatus {

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long idRentalStatus;

  @ManyToOne
  @JoinColumn(name = "id_rental")
  private Rental rental;

  @Column
  private Status status;

  @Column
  private ZonedDateTime date;

  public enum Status {
    INITIATED,
    CONFIRMED,
    COMPLETED,
    CANCELLED;
  }
}
