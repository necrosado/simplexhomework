package org.jslain.homeworks.simplex.core.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ToolInstance {

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long idToolInstance;

  @Column
  private String distinctiveName;

  @Column
  private String distinctiveNameSearch;

  @ManyToOne
  @JoinColumn(name = "id_tool")
  private Tool tool;

  @OneToMany(mappedBy = "toolInstance")
  private List<RentalItem> rentalItems;
}
