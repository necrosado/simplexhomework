package org.jslain.homeworks.simplex.core.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Table
@Data
public class RentalItem {

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long idRentalItem;

  @ManyToOne
  @JoinColumn(name = "id_tool_instance")
  private ToolInstance toolInstance;

  @ManyToOne
  @JoinColumn(name = "id_rental")
  private Rental rental;

  @Column
  private ZonedDateTime startDate;

  @Column
  private ZonedDateTime endDate;
}
