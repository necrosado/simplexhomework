package org.jslain.homeworks.simplex.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.sql.Blob;

@Entity
@Table
@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Image {

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long idImage;

  @ManyToOne
  @JoinColumn(name = "id_tool")
  @ToString.Exclude
  @JsonIgnore
  private Tool tool;

  @Column
  private String filename;

  @Column
  private Integer displayOrder;

  @Lob
  @Column
  @Basic(fetch = FetchType.LAZY)
  private byte[] data;
}
