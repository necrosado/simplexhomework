package org.jslain.homeworks.simplex.core.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Customer {

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long idCustomer;

  @Column
  private String authorizedClientRegistrationId;

  @Column
  private String principalId;

  @Column
  private String firstName;

  @Column
  private String lastName;

  @Column
  private String phoneNumber;

  @Column
  private String address;

  @Column
  private String firstNameSearch;

  @Column
  private String lastNameSearch;

  @Column
  private String phoneNumberSearch;

  @Column
  private String addressSearch;

  @OneToMany(mappedBy = "customer")
  private List<Rental> rentals;
}
