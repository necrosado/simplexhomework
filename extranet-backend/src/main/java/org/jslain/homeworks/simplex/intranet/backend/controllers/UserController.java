package org.jslain.homeworks.simplex.intranet.backend.controllers;

import lombok.extern.slf4j.Slf4j;
import org.jslain.homeworks.simplex.core.entities.Customer;
import org.jslain.homeworks.simplex.intranet.backend.dto.CompleteRegistrationRequest;
import org.jslain.homeworks.simplex.intranet.backend.dto.CustomerDto;
import org.jslain.homeworks.simplex.intranet.backend.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Optional;

@RestController
@RequestMapping("users")
@Slf4j
public class UserController {

  @Autowired
  private OAuth2AuthorizedClientService authorizedClientService;

  @Autowired
  private CustomerRepository customerRepository;

  @GetMapping("current")
  public CustomerDto getCurrentUser(OAuth2AuthenticationToken authentication) {
    CustomerDto customerDto = null;

    if (authentication != null){
      if(authentication.isAuthenticated()){
        Optional<Customer> saved = this.customerRepository.findByAuthorizedClientRegistrationIdAndPrincipalId(
          authentication.getAuthorizedClientRegistrationId(),
          authentication.getName());

        CustomerDto.CustomerDtoBuilder customerBuilder = CustomerDto.builder();

        if(saved.isPresent()){
          Customer savedCustomer = saved.get();

          customerBuilder = customerBuilder.address(savedCustomer.getAddress())
            .phoneNumber(savedCustomer.getPhoneNumber())
            .firstName(savedCustomer.getFirstName())
            .lastName(savedCustomer.getLastName())
            ;
        }

        customerDto = customerBuilder.build();
      }
    }


    return customerDto;
  }

  @GetMapping("post-login")
  public RedirectView postLogin() {
    return new RedirectView ("/post-login");
  }

  @PostMapping("complete-registration")
  public void completeRegistration(@RequestBody CompleteRegistrationRequest req,
                                   OAuth2AuthenticationToken authentication) {
    Customer customer = Customer.builder().principalId(authentication.getName())
      .authorizedClientRegistrationId(authentication.getAuthorizedClientRegistrationId())
      .firstName(req.getFirstName())
      .lastName(req.getLastName())
      .phoneNumber(req.getPhoneNumber())
      .address(req.getAddress())
      .build();

    this.customerRepository.save(customer);
  }
}
