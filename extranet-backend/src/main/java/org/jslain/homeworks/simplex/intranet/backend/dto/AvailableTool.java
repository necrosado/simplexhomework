package org.jslain.homeworks.simplex.intranet.backend.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@Builder
public class AvailableTool {

  private String name;

  private String description;

  private BigDecimal price;

  private long numberAvailable;

}
