package org.jslain.homeworks.simplex.intranet.backend;

import org.jslain.homeworks.simplex.core.entities.Tool;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackageClasses = Tool.class)
public class ExtranetBackendInitialApplication {

	public static void main(String[] args) {
    SpringApplication application = new SpringApplication(ExtranetBackendInitialApplication.class);
    application.setAdditionalProfiles("initial");
    application.run(args);
	}

}
