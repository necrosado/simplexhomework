package org.jslain.homeworks.simplex.intranet.backend.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerDto {
  private String firstName;
  private String lastName;
  private String phoneNumber;
  private String address;
}
