package org.jslain.homeworks.simplex.intranet.backend.config;

import org.apache.ftpserver.config.spring.factorybeans.FtpServerFactoryBean;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;

@Configuration
@EnableGlobalMethodSecurity(
  securedEnabled = true,
  jsr250Enabled = true,
  prePostEnabled = true)
@Order(-50)
@EnableOAuth2Sso
public class ServerConfigSecurity extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .antMatcher("/**")
      .authorizeRequests()
      .antMatchers("/", "/webjars/**", "/error**", /*"/oauth2/**",*/ "/public/**", "/h2-console/**")
      .permitAll()
      .anyRequest()
      .authenticated()
    .and().csrf().disable().logout()
      .invalidateHttpSession(true)
      .permitAll()
    .and().exceptionHandling().authenticationEntryPoint(new Http403ForbiddenEntryPoint())
    .and().oauth2Login()
      .successHandler((req, res, auth) -> {
        res.sendRedirect("/post-login");
      });

    http.headers().frameOptions().sameOrigin();
  }

  @Bean
  public FilterRegistrationBean<OAuth2ClientContextFilter> oauth2ClientFilterRegistration2(OAuth2ClientContextFilter filter) {
    FilterRegistrationBean<OAuth2ClientContextFilter> registration = new FilterRegistrationBean<OAuth2ClientContextFilter>();
    registration.setFilter(filter);
    registration.setOrder(-1000);
    return registration;
  }
}
