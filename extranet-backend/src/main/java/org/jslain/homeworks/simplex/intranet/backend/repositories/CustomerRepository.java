package org.jslain.homeworks.simplex.intranet.backend.repositories;

import org.jslain.homeworks.simplex.core.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

  Optional<Customer> findByAuthorizedClientRegistrationIdAndPrincipalId(String authorizedClientRegistrationId, String principalId);
}
