package org.jslain.homeworks.simplex.intranet.backend.dto;

import lombok.Data;

@Data
public class CompleteRegistrationRequest {
  private String firstName;
  private String lastName;
  private String phoneNumber;
  private String address;
}
