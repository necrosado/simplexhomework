package org.jslain.homeworks.simplex.intranet.backend.controllers;

import org.jslain.homeworks.simplex.core.entities.RentalItem;
import org.jslain.homeworks.simplex.core.entities.ToolInstance;
import org.jslain.homeworks.simplex.intranet.backend.dto.AvailableTool;
import org.jslain.homeworks.simplex.intranet.backend.repositories.ToolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.util.List;
import java.util.stream.Collectors;

@RestController()
@RequestMapping("/public/tools")
public class ToolsManager {

  @Autowired
  private ToolRepository toolRepository;

  @GetMapping()
  @PermitAll
  public List<AvailableTool> get() {
    return toolRepository.findAll()
      .stream()
      .map(t -> AvailableTool.builder()
        .name(t.getName())
        .description(t.getDescription())
        .price(t.getPrice())
        .numberAvailable(
          t.getToolInstances().stream()
            .filter(this::isAvailableToolInstance)
          .count())
        .build())
      .collect(Collectors.toList());
  }

  private boolean isAvailableToolInstance(ToolInstance  ti) {
    return ti.getRentalItems().stream().filter(this::isAlreadyRented).count() == 0;
  }

  private boolean isAlreadyRented(RentalItem ri) {
    return ri.getEndDate() == null;
  }
}
